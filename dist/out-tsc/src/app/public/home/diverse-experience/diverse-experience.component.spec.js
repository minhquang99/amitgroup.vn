import { async, TestBed } from '@angular/core/testing';
import { DiverseExperienceComponent } from './diverse-experience.component';
describe('DiverseExperienceComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DiverseExperienceComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(DiverseExperienceComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=diverse-experience.component.spec.js.map