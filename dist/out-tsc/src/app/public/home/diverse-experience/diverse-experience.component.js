import * as tslib_1 from "tslib";
import { Component, HostListener } from '@angular/core';
import { DemoService } from '../../services/demo.service';
let DiverseExperienceComponent = class DiverseExperienceComponent {
    constructor(demo) {
        this.demo = demo;
        this.currentWidth = 0;
    }
    ngOnInit() {
        this.diverse_experiences = this.demo.diverse_experiences;
    }
    ngAfterViewInit() {
        this.resizeExp();
    }
    resizeExp() {
        document.querySelectorAll('.section-experiences .experience img').forEach((img) => {
            img.onload = () => {
                var exp = img.parentElement;
                exp.style.height = exp.offsetWidth + 'px';
            };
        });
        this.currentWidth = window.innerWidth;
    }
    onresize() {
        if (window.innerWidth != this.currentWidth) {
            this.resizeExp();
        }
    }
};
tslib_1.__decorate([
    HostListener('window:resize'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], DiverseExperienceComponent.prototype, "onresize", null);
DiverseExperienceComponent = tslib_1.__decorate([
    Component({
        selector: 'home-diverse-experience',
        templateUrl: './diverse-experience.component.html',
        styleUrls: ['./diverse-experience.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService])
], DiverseExperienceComponent);
export { DiverseExperienceComponent };
//# sourceMappingURL=diverse-experience.component.js.map