import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
let HomeComponent = class HomeComponent {
    constructor(titleService) {
        this.titleService = titleService;
        this.titleService.setTitle('Homepage - Amitgroup - Transform Digi Together');
    }
    ngOnInit() {
    }
};
HomeComponent = tslib_1.__decorate([
    Component({
        selector: 'app-home',
        templateUrl: './home.component.html',
        styleUrls: ['./home.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [Title])
], HomeComponent);
export { HomeComponent };
//# sourceMappingURL=home.component.js.map