import * as tslib_1 from "tslib";
import { ParticlesService } from './../../services/particles.service';
import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { DemoService } from '../../services/demo.service';
let CoverComponent = class CoverComponent {
    constructor(demo, router, particleService) {
        this.demo = demo;
        this.router = router;
        this.particleService = particleService;
        this.stop = false;
        this.listImages = [];
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                if (event.url != '/') {
                    this.particleService.stop();
                }
            }
        });
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        this.particleService.startParticleSlider(this.canvas_cover.nativeElement, this.section_cover.nativeElement);
        var logo = this.logo_full.nativeElement;
        logo.style.top = (window.innerWidth > 1200) ? ((window.innerHeight) / 920) * 75 - 85 + 94 + 'px' : '50px';
        logo.style.height = (window.innerWidth > 1200) ? ((window.innerHeight - 94) / 920) * 190 + 'px' : '190px';
        logo.style.left = (window.innerWidth > 1200) ? (window.innerWidth / 1618) * 680 + 'px' : '50%';
    }
    slide(event) {
        var span = event.target;
        if (!span.classList.contains('active')) {
            var id = span.getAttribute('data-slide-to');
            // tslint:disable-next-line: radix
            this.particleService.moveSlide(parseInt(id));
            document.querySelectorAll('.cover-slide').forEach((cs) => {
                cs.classList.remove('active');
            });
            document.getElementById('cover-slide-' + id).classList.add('active');
            document.querySelectorAll('.cover-indicators .indicator').forEach((s) => {
                s.classList.remove('active');
            });
            span.classList.add('active');
        }
    }
    onresize() {
        this.logo_full.nativeElement.style.top = (window.innerWidth > 1200) ? ((window.innerHeight) / 920) * 75 - 85 + 94 + 'px' : '50px';
        this.logo_full.nativeElement.style.height = (window.innerWidth > 1200) ? ((window.innerHeight - 94) / 920) * 190 + 'px' : '190px';
        this.logo_full.nativeElement.style.left = (window.innerWidth > 1200) ? (window.innerWidth / 1600) * 680 + 'px' : '50%';
    }
};
tslib_1.__decorate([
    ViewChild('canvas_cover', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], CoverComponent.prototype, "canvas_cover", void 0);
tslib_1.__decorate([
    ViewChild('section_cover', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], CoverComponent.prototype, "section_cover", void 0);
tslib_1.__decorate([
    ViewChild('logo_full', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], CoverComponent.prototype, "logo_full", void 0);
tslib_1.__decorate([
    ViewChild('cover_slider', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], CoverComponent.prototype, "cover_slider", void 0);
tslib_1.__decorate([
    HostListener('window:resize'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], CoverComponent.prototype, "onresize", null);
CoverComponent = tslib_1.__decorate([
    Component({
        selector: 'home-cover',
        templateUrl: './cover.component.html',
        styleUrls: ['./cover.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService, Router, ParticlesService])
], CoverComponent);
export { CoverComponent };
//# sourceMappingURL=cover.component.js.map