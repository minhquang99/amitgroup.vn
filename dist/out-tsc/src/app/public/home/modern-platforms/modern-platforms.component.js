import * as tslib_1 from "tslib";
import { Component, HostListener } from '@angular/core';
import { DemoService } from '../../services/demo.service';
let ModernPlatformsComponent = class ModernPlatformsComponent {
    constructor(demo) {
        this.demo = demo;
        this.currentWidth = 0;
    }
    ngOnInit() {
        this.modern_platforms = this.demo.modern_platforms;
    }
    ngAfterViewInit() {
        this.resizePlatform();
    }
    resizePlatform() {
        document.querySelectorAll('.section-platforms .platform').forEach((platform) => {
            platform.style.height = platform.clientWidth + 'px';
        });
        this.currentWidth = window.innerWidth;
    }
    onresize() {
        if (window.innerWidth != this.currentWidth) {
            this.resizePlatform();
        }
    }
};
tslib_1.__decorate([
    HostListener('window:resize'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], ModernPlatformsComponent.prototype, "onresize", null);
ModernPlatformsComponent = tslib_1.__decorate([
    Component({
        selector: 'home-modern-platforms',
        templateUrl: './modern-platforms.component.html',
        styleUrls: ['./modern-platforms.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService])
], ModernPlatformsComponent);
export { ModernPlatformsComponent };
//# sourceMappingURL=modern-platforms.component.js.map