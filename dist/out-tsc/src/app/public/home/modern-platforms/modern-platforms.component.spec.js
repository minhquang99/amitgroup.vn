import { async, TestBed } from '@angular/core/testing';
import { ModernPlatformsComponent } from './modern-platforms.component';
describe('ModernPlatformsComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ModernPlatformsComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ModernPlatformsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=modern-platforms.component.spec.js.map