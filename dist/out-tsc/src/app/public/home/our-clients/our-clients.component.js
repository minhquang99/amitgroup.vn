import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { DemoService } from '../../services/demo.service';
import Swiper from 'swiper';
let OurClientsComponent = class OurClientsComponent {
    constructor(demo) {
        this.demo = demo;
    }
    ngOnInit() {
        this.our_clients = this.demo.our_clients;
    }
    ngAfterViewInit() {
        var swiper = new Swiper('.swiper-clients', {
            slidesPerView: 2,
            spaceBetween: 30,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            breakpoints: {
                1024: {
                    slidesPerView: 1,
                }
            }
        });
    }
};
OurClientsComponent = tslib_1.__decorate([
    Component({
        selector: 'home-our-clients',
        templateUrl: './our-clients.component.html',
        styleUrls: ['./our-clients.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService])
], OurClientsComponent);
export { OurClientsComponent };
//# sourceMappingURL=our-clients.component.js.map