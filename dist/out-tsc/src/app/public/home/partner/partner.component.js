import * as tslib_1 from "tslib";
import { PartnerService } from './../../services/partner.service';
import { Component } from '@angular/core';
import Swiper from 'swiper';
let PartnerComponent = class PartnerComponent {
    constructor(partnerService) {
        this.partnerService = partnerService;
        this.partners = [];
    }
    ngOnInit() {
        if (this.partnerService.partners.length <= 0) {
            this.partnerService.createDemoPartner().subscribe(pArr => {
                this.partners = pArr;
            });
        }
        else {
            this.partners = this.partnerService.partners;
        }
    }
    ngAfterViewInit() {
        var swiper = new Swiper('.swiper-partner', {
            slidesPerView: 6,
            spaceBetween: 1,
            freeMode: true,
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                },
                768: {
                    slidesPerView: 2,
                }
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }
};
PartnerComponent = tslib_1.__decorate([
    Component({
        selector: 'home-partner',
        templateUrl: './partner.component.html',
        styleUrls: ['./partner.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [PartnerService])
], PartnerComponent);
export { PartnerComponent };
//# sourceMappingURL=partner.component.js.map