import * as tslib_1 from "tslib";
import { Component, Directive, HostListener } from '@angular/core';
import { DemoService } from '../../services/demo.service';
let OurService = class OurService {
    onClick(target) {
        console.log(target);
    }
};
tslib_1.__decorate([
    HostListener('click', ['$event.target']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], OurService.prototype, "onClick", null);
OurService = tslib_1.__decorate([
    Directive({ selector: '[ourservice]' })
], OurService);
let OurServicesComponent = class OurServicesComponent {
    constructor(demo) {
        this.demo = demo;
    }
    ngOnInit() {
        this.our_services = this.demo.our_services;
    }
};
OurServicesComponent = tslib_1.__decorate([
    Component({
        selector: 'home-our-services',
        templateUrl: './our-services.component.html',
        styleUrls: ['./our-services.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService])
], OurServicesComponent);
export { OurServicesComponent };
//# sourceMappingURL=our-services.component.js.map