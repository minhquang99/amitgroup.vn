import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
let PageMaintenanceComponent = class PageMaintenanceComponent {
    constructor(location, titleService) {
        this.location = location;
        this.titleService = titleService;
        this.titleService.setTitle('Maintenance - Amitgroup - Transform Digi Together');
    }
    ngOnInit() {
    }
    backPrevious() {
        this.location.back();
    }
};
PageMaintenanceComponent = tslib_1.__decorate([
    Component({
        selector: 'app-page-maintenance',
        templateUrl: './page-maintenance.component.html',
        styleUrls: ['./page-maintenance.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [Location, Title])
], PageMaintenanceComponent);
export { PageMaintenanceComponent };
//# sourceMappingURL=page-maintenance.component.js.map