import * as tslib_1 from "tslib";
import { Directive, ElementRef, HostListener } from '@angular/core';
let FullpageDirective = class FullpageDirective {
    constructor(el) {
        this.isMobile = false;
        this.sections = [];
        this.number = 0;
        this.current = 0;
        this.wait = false;
        this.Enable = true;
        this.navigators = [];
        this.parent = el.nativeElement;
        if (window.innerWidth > 1024 && this.Enable) {
            this.parent.style.height = '100vh';
            this.parent.style.overflow = 'hidden';
        }
        this.isMobile = /iPhone|iPad|iPod|Android|MeeGo|PlayBook/i.test(navigator.userAgent);
    }
    ngAfterViewInit() {
        if (!this.isMobile && this.Enable) {
            this.parent.querySelectorAll('.fullpage-section .section').forEach((section, index) => {
                this.sections.push(section);
                section.classList.add('section-fullpage');
                section.style.height = window.innerHeight + 'px';
                section.style.transform = `translate(0px, ${(index - this.current) * window.innerHeight}px)`;
            });
            this.number = this.sections.length;
            if (this.number > 0) {
                this.fbNavbar = document.querySelector('.fullpage-navbar');
                var navigators = document.querySelector('.fullpage-navbar .fullpage-navigators');
                this.fbNavbar.style.display = 'block';
                for (let i = 0; i < this.number; i++) {
                    var navigator = document.createElement('DIV');
                    navigator.className = (i == this.current) ? 'fullpage-navigator active' : 'fullpage-navigator';
                    navigator.setAttribute('data-index', i.toString());
                    navigator.innerHTML = '<div class="circle"></div><div class="text">' + this.sections[i].getAttribute('data-title') + '</div>';
                    navigators.appendChild(navigator);
                    this.navigators.push(navigator);
                }
                this.changeNavigator();
                this.sections[0].classList.add('active');
                this.initScrollSeeMore();
                this.sections[this.sections.length - 1].appendChild(this.scrollSeeMore);
            }
        }
        this.btnGoTop = this.parent.querySelector('.btn-top-fixed');
    }
    initScrollSeeMore() {
        this.scrollSeeMore = document.createElement('DIV');
        this.scrollSeeMore.className = 'fullpage-seemore';
        this.scrollSeeMore.innerHTML = '<span>Scroll To See More</span><span><i class="fa fa-angle-double-down"></i></span>';
    }
    onwheel(e) {
        if (!this.isMobile) {
            if (e.deltaY > 0) {
                if (this.current < this.number - 1) {
                    e.preventDefault();
                    if (!this.wait) {
                        this.wait = true;
                        this.current++;
                        this.changeSlide();
                        this.changeNavigator();
                        setTimeout(() => { this.wait = false; }, 1000);
                    }
                }
                if (this.current >= this.number - 1 && this.wait) {
                    e.preventDefault();
                }
                if (this.current >= this.number - 1 && window.scrollY > 0) {
                    this.scrollSeeMore.classList.add('hide');
                    this.fbNavbar.classList.add('hide');
                }
            }
            if (e.deltaY < 0) {
                if (this.current > 0 && window.scrollY == 0) {
                    e.preventDefault();
                    window.scrollTo(0, 0);
                    this.fbNavbar.classList.remove('hide');
                    if (!this.wait) {
                        this.wait = true;
                        this.current--;
                        this.changeSlide();
                        this.changeNavigator();
                        setTimeout(() => { this.wait = false; }, 1000);
                    }
                }
                if (window.scrollY <= 100) {
                    this.fbNavbar.classList.remove('hide');
                }
            }
        }
    }
    changeNavigator() {
        this.navigators.forEach(nav => {
            nav.classList.remove('active');
        });
        this.navigators[this.current].classList.add('active');
    }
    changeSlide() {
        this.sections.forEach((section, index) => {
            section.classList.remove('active');
            section.style.transform = `translate(0px, ${-(this.current) * window.innerHeight}px)`;
        });
        this.sections[this.current].classList.add('active');
    }
    onNavigate(e) {
        if (e.target.closest('.btn-top-fixed')) {
            this.current = 0;
            this.changeNavigator();
            this.changeSlide();
        }
        if (e.target.closest('.fullpage-navbar .fullpage-navigator')) {
            if (!e.target.classList.contains('active')) {
                this.current = parseInt(e.target.getAttribute('data-index'));
                this.changeSlide();
                this.changeNavigator();
            }
        }
    }
    onkeydown(e) {
        if (!this.isMobile && this.Enable) {
            var code = e.keyCode;
            /** Key Down */
            if (code == 34) {
                if (this.current < this.number - 1) {
                    e.preventDefault();
                    this.current++;
                    this.changeSlide();
                    this.changeNavigator();
                }
                else {
                    this.fbNavbar.classList.add('hide');
                }
            }
            /** Key up */
            if (code == 33) {
                if (window.scrollY - window.innerHeight <= 50) {
                    this.fbNavbar.classList.remove('hide');
                }
                if (this.current > 0 && window.scrollY == 0) {
                    e.preventDefault();
                    this.current--;
                    this.changeSlide();
                    this.changeNavigator();
                }
            }
            /** End */
            if (code == 35) {
                if (this.current < this.number - 1) {
                    e.preventDefault();
                    this.current = this.number - 1;
                    this.changeSlide();
                    this.changeNavigator();
                }
            }
            /** Home */
            if (code == 36) {
                if (this.current > 0 && window.scrollY == 0) {
                    e.preventDefault();
                    window.scrollTo(0, 0);
                    this.current = 0;
                    this.changeSlide();
                    this.changeNavigator();
                    this.fbNavbar.classList.remove('hide');
                }
            }
        }
    }
};
tslib_1.__decorate([
    HostListener('wheel', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], FullpageDirective.prototype, "onwheel", null);
tslib_1.__decorate([
    HostListener('window:click', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], FullpageDirective.prototype, "onNavigate", null);
tslib_1.__decorate([
    HostListener('window:keydown', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], FullpageDirective.prototype, "onkeydown", null);
FullpageDirective = tslib_1.__decorate([
    Directive({
        // tslint:disable-next-line: directive-selector
        selector: '[fullpage]'
    }),
    tslib_1.__metadata("design:paramtypes", [ElementRef])
], FullpageDirective);
export { FullpageDirective };
//# sourceMappingURL=fullpage.directive.js.map