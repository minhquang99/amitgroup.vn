import * as tslib_1 from "tslib";
import { ParticlesService } from './services/particles.service';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { RouterAnimation } from './models/router-animation';
let PublicComponent = class PublicComponent {
    constructor(router, particleSv) {
        this.router = router;
        this.particleSv = particleSv;
        this.listImages = [
            {
                src: 'assets/images/head.png',
                drawPosition: 'center',
                random: 'true'
            },
            {
                src: 'assets/images/hand.png',
                drawPosition: 'bottom',
                random: 'false'
            }
        ];
        this.loaded = false;
        this.imageArr = [
            '../../../../assets/images/logo_full.png',
            'assets/images/Icon/angle-right.png',
            'assets/images/Icon/angle-left.png',
            'assets/images/home/useacc.jpg',
            'assets/images/logo.png',
            'quote.png',
            'assets/images/head.png',
            'assets/images/hand.png',
            'assets/images/logo.png',
            'assets/images/Symbol.png',
            'assets/images/banner/1.png',
            'assets/images/banner/2.png',
            'assets/images/banner/3.png',
            'assets/images/banner/4.png',
            'assets/images/banner/5.png',
            'assets/images/services/ios.png',
            'assets/images/services/android.png',
            'assets/images/services/uxui.png',
            'assets/images/services/web.png',
            'assets/images/services/game.png',
            'assets/images/services/embedded.png',
            'assets/images/platforms/web.png',
            'assets/images/platforms/ios.png',
            'assets/images/platforms/android.png',
            'assets/images/platforms/iot.png',
            'assets/images/platforms/ai.png',
            'assets/images/experience/1.png',
            'assets/images/experience/2.png',
            'assets/images/experience/3.png',
            'assets/images/experience/4.png',
            'assets/images/experience/5.png',
            'assets/images/experience/6.png',
            'assets/images/experience/7.png',
            'assets/images/experience/8.png',
            'assets/images/partner/bitis_1.png',
            'assets/images/partner/dhl_1.png',
            'assets/images/partner/klaussner_1.png',
            'assets/images/partner/logistics_1.png',
            'assets/images/partner/mobis_1.png',
            'assets/images/partner/sk_1.png',
            'assets/images/product/head-up_screen.png',
            'assets/images/product/universal_traffic_recorder.png',
            'assets/images/product/tangthuvien.png',
            'assets/images/product/go_application.png',
            'assets/images/product/block_puzzle_jewel.png',
            'assets/images/product/smart_farm.png',
            'assets/images/product/cbmotion.png',
            'assets/images/product/hatoxu.png',
            'assets/images/product/logistics.png',
            'assets/images/product/verveba_telecom.png',
            'assets/images/product/global_vietnam_aupair.png',
            'assets/images/product/topica.png',
            'assets/images/technologies/mobile.png',
            'assets/images/technologies/web.png',
            'assets/images/technologies/database.png',
            'assets/images/technologies/server-os.png',
            'assets/images/technologies/cloud.png',
            'assets/images/technologies/embedded.png',
            'assets/images/technologies/ai.png',
            'assets/images/technologies/blockchain.png',
            'assets/images/technologies/source-code-management-tool.png',
        ];
        this.promiseArr = [];
        this.loadedImg = 0;
        this.sections = [];
        this.isScrolling = false;
        this.DELAY = 1500;
        this.current = 0;
        this.navbarFixed = false;
        (() => tslib_1.__awaiter(this, void 0, void 0, function* () {
            // tslint:disable-next-line: prefer-for-of
            // for (let i = 0; i < this.imageArr.length; i++) {
            // 	var img = await this.loadImage(this.imageArr[i]);
            // }
            this.particleSv.initImageData(this.listImages).then(() => {
                this.loaded = true;
            });
        }))();
        this.router.events.subscribe((evt) => {
            if ((evt instanceof NavigationEnd)) {
                window.scrollTo(0, 0);
            }
        });
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        window.scrollTo(0, 0);
    }
    loadImage(url) {
        return new Promise((resolve, reject) => {
            var img = new Image();
            img.onload = () => {
                this.loadedImg++;
                this.progress();
                resolve();
            };
            img.onerror = () => {
                this.loadedImg++;
                this.progress();
                resolve();
            };
            img.src = url;
        });
    }
    progress() {
        var num = Math.floor((this.loadedImg / this.imageArr.length) * 100);
        this.progressValue.nativeElement.innerText = num + '%';
        this.progressBar.nativeElement.style.width = (200 - Math.floor((num / 100) * 200)) + 'px';
    }
    prepareRoute() { }
};
tslib_1.__decorate([
    ViewChild('progressValue', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], PublicComponent.prototype, "progressValue", void 0);
tslib_1.__decorate([
    ViewChild('progressBar', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], PublicComponent.prototype, "progressBar", void 0);
PublicComponent = tslib_1.__decorate([
    Component({
        selector: 'app-public',
        templateUrl: './public.component.html',
        styleUrls: ['./public.component.scss'],
        animations: [
            RouterAnimation
        ]
    }),
    tslib_1.__metadata("design:paramtypes", [Router, ParticlesService])
], PublicComponent);
export { PublicComponent };
//# sourceMappingURL=public.component.js.map