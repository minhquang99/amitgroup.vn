import * as tslib_1 from "tslib";
import { Component, ChangeDetectorRef } from '@angular/core';
import { SharedService } from '../shared.service';
import { ProductService } from '../services/product.service';
let PageProductComponent = class PageProductComponent {
    constructor(shared, productService, cdRef) {
        this.shared = shared;
        this.productService = productService;
        this.cdRef = cdRef;
        this.products = [];
        this.filtedProducts = [];
        this.filterCat = 0;
    }
    ngOnInit() {
        this.productService.createMockProducts().subscribe(result => { this.products = result; });
        this.categories = this.productService.mockCategory;
    }
    filterCategory(event, id) {
        this.filtedProducts = [];
        this.filterCat = id;
        this.products.forEach((product) => {
            if (product.category == this.filterCat) {
                this.filtedProducts.push(product);
            }
        });
    }
};
PageProductComponent = tslib_1.__decorate([
    Component({
        selector: 'app-page-product',
        templateUrl: './page-product.component.html',
        styleUrls: ['./page-product.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [SharedService, ProductService, ChangeDetectorRef])
], PageProductComponent);
export { PageProductComponent };
//# sourceMappingURL=page-product.component.js.map