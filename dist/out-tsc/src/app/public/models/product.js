export class Product {
    constructor(product) {
        this.id = product.id;
        this.name = product.name;
        this.category = product.category;
        this.image = product.image;
        this.slug = product.slug;
    }
}
//# sourceMappingURL=product.js.map