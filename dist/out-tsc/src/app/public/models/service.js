export class Service {
    constructor(service) {
        this.id = service.id;
        this.name = service.name;
        this.description = service.description;
        this.image = service.image;
        this.image_2 = service.image_2;
    }
}
//# sourceMappingURL=service.js.map