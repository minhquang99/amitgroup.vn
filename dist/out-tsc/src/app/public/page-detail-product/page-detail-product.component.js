import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DemoService } from '../services/demo.service';
let PageDetailProductComponent = class PageDetailProductComponent {
    constructor(activatedRoute, demo) {
        this.activatedRoute = activatedRoute;
        this.demo = demo;
    }
    ngOnInit() {
        var productSlug = this.activatedRoute.snapshot.paramMap.get('slug');
        this.demo.getProduct(productSlug).subscribe(p => { this.product = p; });
    }
};
PageDetailProductComponent = tslib_1.__decorate([
    Component({
        selector: 'app-page-detail-product',
        templateUrl: './page-detail-product.component.html',
        styleUrls: ['./page-detail-product.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [ActivatedRoute, DemoService])
], PageDetailProductComponent);
export { PageDetailProductComponent };
//# sourceMappingURL=page-detail-product.component.js.map