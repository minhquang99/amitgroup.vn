import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MatButtonModule, MatDialogModule } from '@angular/material';
import { PublicRoutingModule } from './public-routing.module';
import { MnFullpageModule } from 'ngx-fullpage';
import { PublicComponent } from './public.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { CoverComponent } from './home/cover/cover.component';
import { ModernPlatformsComponent } from './home/modern-platforms/modern-platforms.component';
import { DiverseExperienceComponent } from './home/diverse-experience/diverse-experience.component';
import { OurServicesComponent } from './home/our-services/our-services.component';
import { OurClientsComponent } from './home/our-clients/our-clients.component';
import { PageProductComponent } from './page-product/page-product.component';
import { PageServiceComponent } from './page-service/page-service.component';
import { PageTechnologyComponent } from './page-technology/page-technology.component';
import { PageContactComponent } from './page-contact/page-contact.component';
import { PageCompanyComponent } from './page-company/page-company.component';
import { MessageComponent } from './layout/message/message.component';
import { PageNotfoundComponent } from './page-notfound/page-notfound.component';
import { PageMaintenanceComponent } from './page-maintenance/page-maintenance.component';
import { PartnerComponent } from './home/partner/partner.component';
import { PageDetailProductComponent } from './page-detail-product/page-detail-product.component';
import { PageBlogComponent } from "./page-blog/page-blog.component";
import { FullpageDirective } from './directive/fullpage.directive';
let PublicModule = class PublicModule {
};
PublicModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            PublicComponent,
            HeaderComponent,
            FooterComponent,
            HomeComponent,
            CoverComponent,
            ModernPlatformsComponent,
            DiverseExperienceComponent,
            OurServicesComponent,
            OurClientsComponent,
            PageProductComponent,
            PageServiceComponent,
            PageTechnologyComponent,
            PageBlogComponent,
            PageContactComponent,
            PageCompanyComponent,
            MessageComponent,
            PageNotfoundComponent,
            PageMaintenanceComponent,
            PartnerComponent,
            PageDetailProductComponent,
            FullpageDirective,
        ],
        imports: [
            CommonModule,
            BrowserModule,
            ReactiveFormsModule,
            HttpClientModule,
            BrowserAnimationsModule,
            AngularFontAwesomeModule,
            MatButtonModule,
            MatDialogModule,
            PublicRoutingModule,
            MnFullpageModule.forRoot()
        ],
        entryComponents: [
            MessageComponent
        ],
        providers: [CoverComponent]
    })
], PublicModule);
export { PublicModule };
//# sourceMappingURL=public.module.js.map