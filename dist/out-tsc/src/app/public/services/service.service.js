import * as tslib_1 from "tslib";
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import { DemoService } from './demo.service';
import { Service } from '../models/service';
let ServiceService = class ServiceService {
    constructor(demo) {
        this.demo = demo;
        this.services = [];
    }
    createDemoServices() {
        if (this.services.length <= 0) {
            this.demo.our_services.services.forEach((service, index) => {
                service["id"] = index;
                this.services.push(new Service(service));
            });
            return of(this.services);
        }
        else {
            return of(this.services);
        }
    }
};
ServiceService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService])
], ServiceService);
export { ServiceService };
//# sourceMappingURL=service.service.js.map