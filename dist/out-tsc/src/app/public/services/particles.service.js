import * as tslib_1 from "tslib";
import { Particles } from './../models/particles';
import { Injectable } from '@angular/core';
const THICKNESS = (window.innerWidth > 1024) ? Math.pow(60, 2) : Math.pow(30, 2);
const COLOR = 255;
let ParticlesService = class ParticlesService {
    constructor() {
        this.imgSrc = '';
        this.arrayData = [];
        this.currentIndex = 0;
        this.radius = 1;
        this.render = () => {
            if (this.canvas.width != window.innerWidth) {
                if (window.innerWidth < 1200 && this.currentIndex != 0) {
                    this.currentIndex = 0;
                    this.particles = this.arrayData[this.currentIndex];
                }
                this.particles.setPosition();
                this.canvas.width = window.innerWidth;
                this.canvas.height = window.innerHeight;
                this.w = this.canvas.width;
                this.h = this.canvas.height;
            }
            // tslint:disable-next-line: no-conditional-assignment
            if (this.tog = !this.tog) {
                if (!this.man) {
                    this.t = +new Date() * 0.001;
                    this.mx = this.w * 0.5 + (Math.cos(this.t * 2.1) * Math.cos(this.t * 0.9) * this.w * 0.45);
                    this.my = this.h * 0.5 + (Math.sin(this.t * 3.2) * Math.tan(Math.sin(this.t * 0.8)) * this.h * 0.45);
                }
                // tslint:disable-next-line: prefer-for-of
                for (let i = 0; i < this.particles.particles.length; i++) {
                    var p = this.particles.particles[i];
                    this.d = (this.dx = this.mx - p.x) * this.dx + (this.dy = this.my - p.y) * this.dy;
                    p.accX = (p.ox - p.x) / 400;
                    p.accY = (p.oy - p.y) / 400;
                    p.vx += p.accX;
                    p.vy += p.accY;
                    p.vx *= p.friction;
                    p.vy *= p.friction;
                    if (this.d < this.radius * THICKNESS) {
                        p.accX = (p.x - this.mx) / (15 * (this.d / (THICKNESS / 2)));
                        p.accY = (p.y - this.my) / (15 * (this.d / (THICKNESS / 2)));
                        p.vx += p.accX;
                        p.vy += p.accY;
                    }
                    else {
                        p.accX = Math.random() / 10;
                        p.accY = Math.random() / 10;
                        p.vx += p.accX;
                        p.vy += p.accY;
                    }
                    p.x += p.vx;
                    p.y += p.vy;
                }
            }
            else {
                this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                this.b = (this.a = this.ctx.createImageData(this.w, this.h)).data;
                // tslint:disable-next-line: prefer-for-of
                for (let i = 0; i < this.particles.particles.length; i++) {
                    var p = this.particles.particles[i];
                    // tslint:disable-next-line: no-bitwise
                    var n = (~~(p.x + this.particles.left) + (~~(p.y + this.particles.top) * this.w)) * 4;
                    this.b[n] = this.b[n + 1] = this.b[n + 2] = COLOR, this.b[n + 3] = 255;
                }
                this.ctx.putImageData(this.a, this.particles.translateX, this.particles.translateY);
                // var left = this.particles.left;
                // var top = this.particles.top;
                // for (let i = 0; i < this.particles.particles.length; i++) {
                //   var p = this.particles.particles[i];
                //   this.ctx.drawImage(this.sparkle, p.x + left, p.y + top, 4, 4);
                // }
            }
            this.raf = window.requestAnimationFrame(this.render);
        };
    }
    /** Khởi tạo dữ liệu: Model - Canvas - Container */
    initImageData(images) {
        return new Promise((resolve, reject) => {
            var canvas = document.createElement('CANVAS');
            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;
            if (this.arrayData.length <= 0) {
                // tslint:disable-next-line: no-unused-expression
                (() => tslib_1.__awaiter(this, void 0, void 0, function* () {
                    // tslint:disable-next-line: prefer-for-of
                    for (let i = 0; i < images.length; i++) {
                        var image = images[i];
                        var particles = new Particles();
                        var result = yield particles.initImageData(image, canvas);
                        particles.setPosition();
                        this.arrayData.push(particles);
                    }
                    resolve();
                }))();
            }
        });
    }
    startParticleSlider(canvas, container) {
        this.container = container;
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.w = this.canvas.width;
        this.h = this.canvas.height;
        this.canvas.style.display = 'block';
        this.addEvent();
        this.setupData();
        this.render();
    }
    addEvent() {
        this.container.addEventListener('mousemove', (e) => {
            var bounds = this.container.getBoundingClientRect();
            this.mx = e.clientX - bounds.left - this.particles.left;
            this.my = e.clientY - bounds.top - this.particles.top - this.particles.translateY;
            this.man = true;
        });
        this.container.addEventListener('click', (e) => {
            this.radius++;
            if (this.radius > 5) {
                this.radius = 1;
            }
        });
    }
    setupData() {
        this.particles = this.arrayData[this.currentIndex];
        this.particles.setPosition();
        this.tog = true;
        this.man = false;
    }
    moveSlide(index) {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.currentIndex = index;
        this.setupData();
    }
    stop() {
        window.cancelAnimationFrame(this.raf);
    }
};
ParticlesService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [])
], ParticlesService);
export { ParticlesService };
//# sourceMappingURL=particles.service.js.map