import * as tslib_1 from "tslib";
import { DemoService } from './demo.service';
import { Partner } from './../models/partner';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
let PartnerService = class PartnerService {
    constructor(demo) {
        this.demo = demo;
        this.partners = [];
    }
    createDemoPartner() {
        this.demo.partners.forEach(partner => {
            var p = new Partner(partner);
            this.partners.push(p);
        });
        return of(this.partners);
    }
};
PartnerService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService])
], PartnerService);
export { PartnerService };
//# sourceMappingURL=partner.service.js.map