import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';
let BreadcrumbService = class BreadcrumbService {
    constructor(router, activatedRoute, BCService) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.BCService = BCService;
        this.navigationEnd$ = this.router.events.pipe(filter(event => event instanceof NavigationEnd), map(event => this.buildBreadCrumb(this.activatedRoute.root)));
    }
    buildBreadCrumb(route, url = '', breadcrumbs = []) {
        const label = route.routeConfig ? route.routeConfig.data['breadcrumb'] : 'Home';
        const path = route.routeConfig ? route.routeConfig.path : '';
        const nextUrl = `${url}${path}/`;
        const breadcrumb = {
            label: label,
            url: nextUrl
        };
        const newBreadCrumbs = [...breadcrumbs, breadcrumb];
        if (route.firstChild) {
            return this.buildBreadCrumb(route.firstChild.firstChild, nextUrl, newBreadCrumbs);
        }
        return newBreadCrumbs;
    }
};
BreadcrumbService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [Router, ActivatedRoute, BreadcrumbService])
], BreadcrumbService);
export { BreadcrumbService };
//# sourceMappingURL=breadcrumb.service.js.map