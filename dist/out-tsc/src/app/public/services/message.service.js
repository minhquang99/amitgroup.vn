import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { MessageComponent } from '../layout/message/message.component';
import { MatDialog } from '@angular/material';
let MessageService = class MessageService {
    constructor(dialog) {
        this.dialog = dialog;
    }
    openDialog(title, message) {
        this.dialogRef = this.dialog.open(MessageComponent, { width: '300px' });
        this.dialogRef.componentInstance.title = title;
        this.dialogRef.componentInstance.message = message;
        return this.dialogRef.afterClosed();
    }
};
MessageService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [MatDialog])
], MessageService);
export { MessageService };
//# sourceMappingURL=message.service.js.map