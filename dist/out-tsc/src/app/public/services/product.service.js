import * as tslib_1 from "tslib";
import { DemoService } from './demo.service';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Product } from '../models/product';
let ProductService = class ProductService {
    constructor(demo) {
        this.demo = demo;
        this.products = [];
        this.mockCategory = [
            {
                id: 1,
                name: 'E-Commerce',
            },
            {
                id: 2,
                name: 'Education'
            },
            {
                id: 3,
                name: 'Healthcare'
            },
            {
                id: 4,
                name: 'Hospital',
            },
            {
                id: 5,
                name: 'Media & Entertainment',
            },
            {
                id: 6,
                name: 'Telecom & Networking'
            },
            {
                id: 7,
                name: 'Automotive Services & Dealerships'
            },
            {
                id: 8,
                name: 'Fintech & Commodity Exchange Trading'
            }
        ];
    }
    createMockProducts() {
        if (this.products.length <= 0) {
            // tslint:disable-next-line: prefer-for-of
            for (let i = 0; i < this.demo.products.length; i++) {
                var product = this.demo.products[i];
                this.products.push(new Product({
                    id: product.id,
                    name: product.name,
                    slug: product.slug,
                    category: product.category,
                    image: product.image
                }));
            }
            return of(this.products);
        }
        else {
            return of(this.products);
        }
    }
};
ProductService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService])
], ProductService);
export { ProductService };
//# sourceMappingURL=product.service.js.map