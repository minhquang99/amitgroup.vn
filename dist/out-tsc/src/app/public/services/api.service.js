import * as tslib_1 from "tslib";
import { of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageService } from './message.service';
let ApiService = class ApiService {
    // setToken() {
    //   this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + 'Token');
    // }
    constructor(http, messageService) {
        this.http = http;
        this.messageService = messageService;
        this.apiUrl = window.location.origin + '/api/';
        this.httpOptions = {
        // headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
        };
    }
    sendMail(url, data) {
        return this.http.post(this.apiUrl + url, data, this.httpOptions).pipe(map((result) => {
            return (result);
        }), catchError(this.handleError('Error', false)));
    }
    handleError(operation = 'operation', result) {
        return (error) => {
            this.messageService.openDialog('Error Occured', error.status + '\n' + error.error.message);
            return of(result);
        };
    }
};
ApiService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient, MessageService])
], ApiService);
export { ApiService };
//# sourceMappingURL=api.service.js.map