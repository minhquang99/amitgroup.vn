import * as tslib_1 from "tslib";
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
let SharedService = class SharedService {
    constructor() {
        this.COMPANY_NAME = 'Amitgroup';
        this.COMPANY_ADDRESS = '181 Dinh Tien Hoang, DaKao Ward, District 1, 70000, Ho Chi Minh city, Vietnam';
        this.COMPANY_DESCRIPTION = 'AMIT Group provides information technology solutions and all-inclusive design services';
        this.COMPANY_EMAIL = 'contact@amitgroup.vn';
        this.COMPANY_PHONE = '(+84) 287 101 7979';
        this.COMPANY_LOGAN = 'Transform Digi Together';
        this.logo = {
            url: '../../../assets/images/logo.png',
            alt: 'Logo'
        };
        this.menu = [
            {
                name: 'Home',
                url: '/',
            },
            {
                name: 'Products',
                url: '/product',
            },
            {
                name: 'Service',
                url: '/service',
            },
            {
                name: 'Technology',
                url: '/technology'
            },
            {
                name: 'Blog',
                url: '/blog'
            },
            {
                name: 'Career',
                url: '/career'
            },
            {
                name: 'Company',
                url: '/company'
            },
            {
                name: 'Contact',
                url: '/contact'
            }
        ];
        this.our_services = {
            header: 'Our Services',
            description: 'We offer web development, iOS and Android services together with all-inclusive design services. We have all the expertises you need to produce friendly, beautiful,stable, and scalable products.',
            services: [
                {
                    name: 'iOS Development',
                    icon: '../../../assets/images/Symbol.png',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
                },
                {
                    name: 'Android Development',
                    icon: '../../../assets/images/Symbol.png',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
                },
                {
                    name: 'UX/UI Designer',
                    icon: '../../../assets/images/Symbol.png',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
                },
                {
                    name: 'Web Development',
                    icon: '../../../assets/images/Symbol.png',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
                },
                {
                    name: 'Game Development',
                    icon: '../../../assets/images/Symbol.png',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
                },
                {
                    name: 'Embedded Software',
                    icon: '../../../assets/images/Symbol.png',
                    description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
                },
            ]
        };
        this.our_clients = {
            header: 'Our Clients Say',
            description: 'Our Client Description',
            clients: [
                {
                    image: '../../../assets/images/Symbol.png',
                    name: 'Client Name',
                    position: 'Client Position',
                    description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
                },
                {
                    image: '../../../assets/images/Symbol.png',
                    name: 'Client Name',
                    position: 'Client Position',
                    description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
                },
                {
                    image: '../../../assets/images/Symbol.png',
                    name: 'Client Name',
                    position: 'Client Position',
                    description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
                },
                {
                    image: '../../../assets/images/Symbol.png',
                    name: 'Client Name',
                    position: 'Client Position',
                    description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
                },
            ]
        };
        this.modern_platforms = {
            header: 'Modern Platforms',
            description: 'A software is good when it can be run on many platforms. We are trying our best to response all our client\'s demand a wide range of services covers many platforms.',
            platforms: [
                {
                    name: 'Name Platform',
                    icon: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Name Platform',
                    icon: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Name Platform',
                    icon: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Name Platform',
                    icon: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Name Platform',
                    icon: '../../../assets/images/Symbol.png'
                },
            ]
        };
        this.diverse_experiences = {
            header: 'We Have Diverse Experience',
            description: 'We offer web development, iOS and Android services together with all-inclusive design services. We have all the expertises you need to produce friendly, beautiful,stable, and scalable products.',
            experiences: [
                {
                    name: 'Experience Name',
                    image: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Experience Name',
                    image: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Experience Name',
                    image: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Experience Name',
                    image: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Experience Name',
                    image: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Experience Name',
                    image: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Experience Name',
                    image: '../../../assets/images/Symbol.png'
                },
                {
                    name: 'Experience Name',
                    image: '../../../assets/images/Symbol.png'
                },
            ]
        };
    }
    getMenu() {
        return of(this.menu);
    }
    getLogo() {
        return of(this.logo);
    }
};
SharedService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [])
], SharedService);
export { SharedService };
//# sourceMappingURL=shared.service.js.map