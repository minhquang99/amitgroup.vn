import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PublicComponent } from './public.component';
import { HomeComponent } from './home/home.component';
import { PageProductComponent } from './page-product/page-product.component';
import { PageServiceComponent } from './page-service/page-service.component';
import { PageTechnologyComponent } from './page-technology/page-technology.component';
import { PageContactComponent } from './page-contact/page-contact.component';
import { PageCompanyComponent } from './page-company/page-company.component';
import { PageNotfoundComponent } from './page-notfound/page-notfound.component';
import { PageMaintenanceComponent } from './page-maintenance/page-maintenance.component';
import { PageDetailProductComponent } from './page-detail-product/page-detail-product.component';
import { PageBlogComponent } from "./page-blog/page-blog.component";
import { PageCareerComponent } from "./page-career/page-career.component";
const publicRoute = [
    {
        path: '',
        component: PublicComponent,
        data: {
            breadcrumb: 'Home'
        },
        children: [
            {
                path: '',
                component: HomeComponent,
                data: {
                    breadcrumb: 'Home',
                    animation: 'Home'
                }
            },
            {
                path: 'product',
                component: PageProductComponent,
                data: {
                    breadcrumb: 'Product',
                    animation: 'Product'
                }
            },
            {
                path: 'product/:slug',
                component: PageDetailProductComponent,
                data: {
                    breadcrumb: 'Product Detail',
                    animation: 'Product Detail'
                }
            },
            {
                path: 'service',
                component: PageServiceComponent,
                data: {
                    breadcrumb: 'Service',
                    animation: 'Service'
                }
            },
            {
                path: 'technology',
                component: PageTechnologyComponent,
                data: {
                    breadcrumb: 'Technology',
                    animation: 'Technology'
                }
            },
            {
                path: 'blog',
                component: PageBlogComponent,
                data: {
                    breadcrumb: 'Blog',
                    animation: 'Blog'
                }
            },
            {
                path: 'career',
                component: PageCareerComponent,
                data: {
                    breadcrumb: 'Career',
                    animation: 'Career'
                }
            },
            {
                path: 'contact',
                component: PageContactComponent,
                data: {
                    breadcrumb: 'Contact',
                    animation: 'Contact'
                }
            },
            {
                path: 'about-us',
                component: PageCompanyComponent,
                data: {
                    breadcrumb: 'About Us',
                    animation: 'About Us'
                }
            },
        ]
    },
    {
        path: 'maintenance',
        component: PageMaintenanceComponent,
        data: {
            breadcrumb: 'Maintenance',
            animation: 'Maintenance'
        }
    },
    {
        path: '**',
        component: PageNotfoundComponent,
        data: {
            breadcrumb: '404',
            animation: '404'
        }
    },
];
let PublicRoutingModule = class PublicRoutingModule {
};
PublicRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(publicRoute)],
        exports: [RouterModule]
    })
], PublicRoutingModule);
export { PublicRoutingModule };
//# sourceMappingURL=public-routing.module.js.map