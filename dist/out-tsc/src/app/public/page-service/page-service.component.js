import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ServiceService } from '../services/service.service';
let PageServiceComponent = class PageServiceComponent {
    constructor(serviceService) {
        this.serviceService = serviceService;
        this.services = [];
    }
    ngOnInit() {
        if (this.services.length <= 0) {
            this.serviceService.createDemoServices().subscribe(services => {
                this.services = services;
            });
        }
    }
};
PageServiceComponent = tslib_1.__decorate([
    Component({
        selector: 'app-page-service',
        templateUrl: './page-service.component.html',
        styleUrls: ['./page-service.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [ServiceService])
], PageServiceComponent);
export { PageServiceComponent };
//# sourceMappingURL=page-service.component.js.map