import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { DemoService } from '../../services/demo.service';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { Title } from '@angular/platform-browser';
let HeaderComponent = class HeaderComponent {
    constructor(demo, BcService, titleService) {
        this.demo = demo;
        this.BcService = BcService;
        this.titleService = titleService;
        this.isHome = true;
        this.navbarHide = false;
        this.menuState = (window.innerWidth > 1024) ? 'desktop' : 'hide';
        this.BcService.navigationEnd$.subscribe(breadscrumbs => {
            this.breadcrumbs = breadscrumbs;
            this.title = (this.breadcrumbs[this.breadcrumbs.length - 1].label == 'About Us') ? 'Our Leadership' : this.breadcrumbs[this.breadcrumbs.length - 1].label;
            this.titleService.setTitle(this.title + ' - Amitgroup - Transform Digi Together');
            if (this.breadcrumbs[this.breadcrumbs.length - 1].label != 'Home') {
                this.isHome = true;
            }
            else {
                this.isHome = false;
            }
            if (this.menuState == 'show') {
                this.menuState = 'hide';
            }
        });
    }
    ngOnInit() {
        this.demo.getMenu().subscribe(menu => {
            this.menu = menu;
        });
        this.demo.getLogo().subscribe(logo => this.logo = logo);
    }
    // tslint:disable-next-line: use-lifecycle-interface
    ngAfterViewInit() {
        if (window.innerWidth > 1024 && this.breadcrumb_bar && window.document.scrollingElement.scrollTop > 30) {
            this.breadcrumb_bar.nativeElement.style.marginTop = this.navbar.nativeElement.offsetHeight + 'px';
        }
    }
    onWindowScroll(event) {
        if (window.innerWidth > 1024) {
            if (!document.querySelector('.fullpage-section')) {
                if (window.scrollY > 30) {
                    this.navbar.nativeElement.classList.add('fixed-top');
                    if (this.breadcrumb_bar) {
                        // this.breadcrumb_bar.nativeElement.style.marginTop = this.navbar.nativeElement.offsetHeight + 'px';
                    }
                }
                else {
                    this.navbar.nativeElement.classList.remove('fixed-top');
                    if (this.breadcrumb_bar) {
                        this.breadcrumb_bar.nativeElement.style.marginTop = '94px';
                    }
                }
            }
        }
    }
    onWheel(event) {
        if (window.innerWidth > 1024) {
            if (document.querySelector('.fullpage-section')) {
                if (event.deltaY > 0 && !this.navbar.nativeElement.classList.contains('fixed-top')) {
                    this.navbar.nativeElement.classList.add('fixed-top');
                }
                if (event.deltaY < 0 && this.navbar.nativeElement.classList.contains('fixed-top') && document.querySelectorAll('.fullpage-section .section')[0].classList.contains('active')) {
                    this.navbar.nativeElement.classList.remove('fixed-top');
                }
            }
        }
    }
    onkeydown(e) {
        if (window.innerWidth > 1024 && document.querySelector('.fullpage-section')) {
            var code = e.keyCode;
            /** Key Down */
            if (code == 34) {
                this.navbar.nativeElement.classList.add('fixed-top');
            }
            /** Key up */
            if (code == 33) {
                if (window.scrollY == 0 && this.navbar.nativeElement.classList.contains('fixed-top') && document.querySelector('.fullpage-section .section-our-services.current')) {
                    this.navbar.nativeElement.classList.remove('fixed-top');
                }
            }
            /** End */
            if (code == 35) {
                this.navbar.nativeElement.classList.add('fixed-top');
            }
            /** Home */
            if (code == 36) {
                if (window.scrollY == 0 && this.navbar.nativeElement.classList.contains('fixed-top')) {
                    this.navbar.nativeElement.classList.remove('fixed-top');
                }
            }
        }
    }
    onWindowResize(event) {
        if (this.menuState != 'desktop' && window.innerWidth > 1024) {
            this.menuState = 'desktop';
        }
        if (this.menuState == 'desktop' && window.innerWidth < 1024) {
            this.menuState = 'hide';
        }
    }
    onWindowClick(event) {
        if (window.innerWidth <= 1024) {
            if (!event.target.closest('.nav.navbar')) {
                if (this.menuState == 'show') {
                    this.menuState = 'hide';
                }
            }
        }
    }
    toggleMenu(event) {
        if (window.innerWidth <= 1024) {
            this.menuState = (this.menuState == 'hide') ? 'show' : 'hide';
        }
        else {
            this.menuState = 'show';
        }
    }
};
tslib_1.__decorate([
    ViewChild('main_menu', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], HeaderComponent.prototype, "mainMenu", void 0);
tslib_1.__decorate([
    ViewChild('navbar', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], HeaderComponent.prototype, "navbar", void 0);
tslib_1.__decorate([
    ViewChild('breadcrumb_bar', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], HeaderComponent.prototype, "breadcrumb_bar", void 0);
tslib_1.__decorate([
    HostListener('window:scroll', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], HeaderComponent.prototype, "onWindowScroll", null);
tslib_1.__decorate([
    HostListener('window:wheel', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], HeaderComponent.prototype, "onWheel", null);
tslib_1.__decorate([
    HostListener('window:keydown', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], HeaderComponent.prototype, "onkeydown", null);
tslib_1.__decorate([
    HostListener('window:resize', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], HeaderComponent.prototype, "onWindowResize", null);
tslib_1.__decorate([
    HostListener('window:click', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], HeaderComponent.prototype, "onWindowClick", null);
HeaderComponent = tslib_1.__decorate([
    Component({
        selector: 'public-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.scss'],
        animations: [
            trigger('menu', [
                state('desktop', style({
                    height: 'auto',
                    opacity: 1
                })),
                state('show', style({
                    height: '360px',
                    opacity: '1',
                })),
                state('hide', style({
                    height: '0px',
                    opacity: '0'
                })),
                transition('hide => show', [
                    animate('300ms ease-out')
                ]),
                transition('show => hide', [
                    animate('300ms ease-out')
                ])
            ])
        ]
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService, BreadcrumbService, Title])
], HeaderComponent);
export { HeaderComponent };
//# sourceMappingURL=header.component.js.map