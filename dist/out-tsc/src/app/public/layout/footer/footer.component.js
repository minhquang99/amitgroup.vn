import * as tslib_1 from "tslib";
import { Component, ViewChild, HostListener, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { DemoService } from '../../services/demo.service';
import { MessageService } from '../../services/message.service';
import { ApiService } from '../../services/api.service';
let FooterComponent = class FooterComponent {
    constructor(demo, api, messageService) {
        this.demo = demo;
        this.api = api;
        this.messageService = messageService;
        this.isClick = false;
        this.minStep = 10;
        this.maxValue = 10000;
        this.minValue = 0;
        this.startX = 0;
        this.isSending = false;
        this.servicesArray = [
            { id: 'ux_ui', name: 'UX / UI' },
            { id: 'design', name: 'Graphic Design' },
            { id: 'ios', name: 'iOS' },
            { id: 'android', name: 'Android' },
            { id: 'web', name: 'Web Development' },
            { id: 'other', name: 'Other' }
        ];
        this.servicesControl = this.servicesArray.map(service => new FormControl(false));
        this.contactForm = new FormGroup({
            name: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(2)
            ])),
            email: new FormControl('', Validators.compose([
                Validators.required,
                this.formatEmail
            ])),
            phone: new FormControl('', Validators.compose([
                Validators.required,
                this.formatPhone
            ])),
            message: new FormControl('', Validators.compose([])),
            budget: new FormControl(0, Validators.compose([
                Validators.required
            ])),
            file: new FormControl('', Validators.compose([
                this.formatFile
            ])),
            services: new FormArray(this.servicesControl)
        });
    }
    ngOnInit() {
    }
    onSubmit(formdata) {
        if (!this.isSending) {
            if (this.contactForm.invalid) {
                var message = '';
                for (const name in this.contactForm.controls) {
                    var control = this.contactForm.controls[name];
                    if (control.invalid) {
                        control.touched = true;
                        message += `${name} is invalid.\n`;
                    }
                }
                this.messageService.openDialog('Error', message).subscribe();
                return false;
            }
            this.isSending = true;
            var selectServices = formdata.services.map((val, index) => { if (val) {
                return this.servicesArray[index].name;
            } }).filter(v => v);
            var data = new FormData();
            data.set('name', formdata.name);
            data.set('email', formdata.email);
            data.set('phone', formdata.phone);
            data.set('budget', formdata.budget.toString());
            data.set('message', formdata.message);
            selectServices.forEach((service) => {
                data.append('services[]', service);
            });
            var fileUpload = document.getElementById('attach_file');
            var file = fileUpload.files[0];
            if (file) {
                data.append('file', file, file.name);
            }
            this.api.sendMail('customer-contact', data).subscribe(result => {
                if (result) {
                    this.messageService.openDialog('Success', result.message);
                    this.resetFormData();
                }
                this.isSending = false;
            });
        }
    }
    // tslint:disable-next-line: use-lifecycle-interface
    ngAfterViewInit() {
        this.btnGoTop = document.querySelector('.btn-top-fixed');
    }
    resetFormData() {
        this.contactForm.get('name').setValue('');
        this.contactForm.get('name').touched = false;
        this.contactForm.get('email').setValue('');
        this.contactForm.get('email').touched = false;
        this.contactForm.get('phone').setValue('');
        this.contactForm.get('phone').touched = false;
        this.contactForm.get('message').setValue('');
        this.contactForm.get('message').touched = false;
        this.contactForm.get('budget').setValue(0);
        this.budget_thumb.nativeElement.style.left = '0px';
        this.contactForm.get('file').setValue('');
        this.contactForm.controls.services.controls.forEach((ctr, i) => {
            this.contactForm.get('services').get(i.toString()).setValue(false);
        });
        this.servicesArray.forEach(service => {
            var input = document.getElementById(service.id);
            input.checked = false;
            var parent = input.parentNode;
            parent.classList.remove('checked');
        });
    }
    formatEmail(control) {
        var email = control.value.trim();
        if (email != '') {
            if (!email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)) {
                return {
                    formatEmailInvalid: true
                };
            }
        }
    }
    formatPhone(control) {
        var phone = control.value.trim();
        var regex = /^\+[1-9]{1}[0-9]{3,14}$/;
        if (phone != '') {
            if (!phone.match(regex)) {
                return {
                    formatPhoneInvalid: true
                };
            }
        }
    }
    formatFile(control) {
        var fileUpload = document.getElementById('attach_file');
        if (fileUpload && fileUpload.value != '') {
            var allowFile = ['png', 'jpg', 'jpeg', 'pdf', 'docx', 'doc', 'excel'];
            var result = 0;
            allowFile.forEach((allow) => {
                if (fileUpload.value.toLowerCase().match(allow)) {
                    result = result + 1;
                }
            });
            if (result <= 0) {
                return { formatFileUpload: true };
            }
        }
    }
    onServiceChange(event) {
        let input = event.target;
        if (input.checked) {
            input.parentNode.classList.add('checked');
        }
        else {
            input.parentNode.classList.remove('checked');
        }
    }
    slideStart(e) {
        this.isClick = true;
        this.startX = this.getX(e, this.budget_bar.nativeElement);
        e.target.classList.add('moving');
    }
    onMouseMove(e) {
        if (this.isClick) {
            var bar = this.budget_bar.nativeElement;
            var thumb = this.budget_thumb.nativeElement;
            var barRect = bar.getBoundingClientRect();
            var x = this.getX(e, bar);
            var rate = (x / barRect.width);
            var value = Math.ceil(rate * this.maxValue);
            thumb.style.left = (x) + 'px';
            this.startX = x;
            this.contactForm.get('budget').setValue(value);
        }
    }
    onTouchMove(e) {
        if (this.isClick) {
            var bar = this.budget_bar.nativeElement;
            var thumb = this.budget_thumb.nativeElement;
            var barRect = bar.getBoundingClientRect();
            var x = this.getX(e, bar);
            var rate = (x / barRect.width);
            var value = Math.ceil(rate * this.maxValue);
            thumb.style.left = x + 'px';
            this.startX = x;
            this.contactForm.get('budget').setValue(value);
        }
    }
    getX(e, bar) {
        let rect = bar.getBoundingClientRect();
        let x = (e.type == 'mousedown' || e.type == 'mousemove') ? e.pageX - rect.left : e.changedTouches[0].pageX - rect.left;
        if (x < 0) {
            x = 0;
        }
        if (x > rect.width) {
            x = rect.width;
        }
        return x;
    }
    slideEnd(e) {
        if (this.isClick) {
            this.isClick = false;
            this.budget_thumb.nativeElement.classList.remove('moving');
        }
    }
    onWindowScroll(event) {
        if (window.scrollY >= (window.document.scrollingElement.scrollHeight - window.innerHeight)) {
            this.btnGoTop.classList.add('show');
        }
        else {
            this.btnGoTop.classList.remove('show');
        }
    }
    scrollToTop(e) {
        if (e.target.closest('.btn-top-fixed')) {
            (function smoothscroll() {
                var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
                if (currentScroll > 0) {
                    window.requestAnimationFrame(smoothscroll);
                    window.scrollTo(0, currentScroll - 100);
                }
            })();
        }
    }
};
tslib_1.__decorate([
    ViewChild('budget_bar', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], FooterComponent.prototype, "budget_bar", void 0);
tslib_1.__decorate([
    ViewChild('budget_thumb', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], FooterComponent.prototype, "budget_thumb", void 0);
tslib_1.__decorate([
    HostListener('window:mousemove', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], FooterComponent.prototype, "onMouseMove", null);
tslib_1.__decorate([
    HostListener('window:touchmove', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], FooterComponent.prototype, "onTouchMove", null);
tslib_1.__decorate([
    HostListener('window:mouseup', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], FooterComponent.prototype, "slideEnd", null);
tslib_1.__decorate([
    HostListener('window:scroll', ['$event']),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", void 0)
], FooterComponent.prototype, "onWindowScroll", null);
FooterComponent = tslib_1.__decorate([
    Component({
        selector: 'public-footer',
        templateUrl: './footer.component.html',
        styleUrls: ['./footer.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [DemoService, ApiService, MessageService])
], FooterComponent);
export { FooterComponent };
//# sourceMappingURL=footer.component.js.map