import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material';
let MessageComponent = class MessageComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
MessageComponent = tslib_1.__decorate([
    Component({
        selector: 'app-message',
        templateUrl: './message.component.html',
        styleUrls: ['./message.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [MatDialogRef])
], MessageComponent);
export { MessageComponent };
//# sourceMappingURL=message.component.js.map