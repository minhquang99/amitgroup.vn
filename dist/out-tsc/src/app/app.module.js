import * as tslib_1 from "tslib";
/** Import Module */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/** Custom Module */
import { AppRoutingModule } from './app-routing.module';
import { PublicRoutingModule } from './public/public-routing.module';
import { PublicModule } from './public/public.module';
/** Import Components */
import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            AppComponent,
            DemoComponent,
        ],
        imports: [
            BrowserModule,
            AppRoutingModule,
            PublicRoutingModule,
            PublicModule
        ],
        providers: [],
        bootstrap: [AppComponent],
        entryComponents: []
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map