import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
let WebglComponent = class WebglComponent {
    constructor() { }
    ngOnInit() {
    }
    ngAfterViewInit() {
        this.drawWebGL();
    }
    drawWebGL() {
        var canvas = this.canvas.nativeElement;
        var gl = canvas.getContext('webgl');
        if (gl === null)
            alert('Unable to initialize WebGL. Your browser or machine may not support it.');
        console.log(gl);
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        const vsSource = `
			attribute vec4 aVertexPosition;
			uniform mat4 uModelViewMatrix;
			uniform mat4 uProjectionMatrix;

			void main() {
				gl_Position = uProjectionMatrix * uModelViewMatrix * uaVertexPosition;
			}
		`;
        const fsSource = `
			void main() {
				gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0)
			}
		`;
        const shaderProgram = initShaderProgram(gl, vsSource, fsSource);
        const programInfo = {
            program: shaderProgram,
            attribLocations: {
                vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
            },
            uniformLocations: {
                projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
                modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix')
            }
        };
        /** -----------------------------------------------------------//-------------------------------------------------------------- */
        // Initialize a shader program, so WebGL knows how to draw our data
        function initShaderProgram(gl, vsSource, fsSource) {
            const vertexShader = loadShader(gl, gl.VERTEX_SHARER, vsSource);
            const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);
            // Create the shader program
            const shaderProgram = gl.createProgram();
            gl.attachShader(shaderProgram, vertexShader);
            gl.attachShader(shaderProgram, fragmentShader);
            gl.linkProgram(shaderProgram);
            // If creating Shader Program failed, alert
            if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
                alert('Unable to initialize the shader program : ' + gl.getProgramInfoLog(shaderProgram));
                return null;
            }
            return shaderProgram;
        }
        // Create a shader of the given type, uploads the source and compiles it.
        function loadShader(gl, type, source) {
            const shader = gl.createShader(type);
            // Send the source to the shader object
            gl.compileShader(shader);
            // See if it compiled successfully
            if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                alert('An error occured compiling the shaders: ' + gl.getShaderInfoLog(shader));
                gl.deleteShader(shader);
                return null;
            }
            return shader;
        }
        /** -----------------------------------------------------------//-------------------------------------------------------------- */
        // Creating the square plane
        function initBuffers(gl) {
            // Create a buffer for the square's position
            const positionBuffer = gl.createBuffer();
            // Select the positionBuffer as the on to apply buffer
            //operations to from here out.
            gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
            const positions = [
                -1.0, 1.0,
                1.0, 1.0,
                -1.0, -1.0,
                1.0, -1.0
            ];
            // Now pass the list of position into WebGL to build the shape. 
            //We do this by creating a Float32Array from the Javascript array,
            // then use it to fill the current buffer
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);
            return {
                position: positionBuffer
            };
        }
    }
};
tslib_1.__decorate([
    ViewChild('webGLDemo', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], WebglComponent.prototype, "canvas", void 0);
WebglComponent = tslib_1.__decorate([
    Component({
        selector: 'app-webgl',
        templateUrl: './webgl.component.html',
        styleUrls: ['./webgl.component.scss']
    }),
    tslib_1.__metadata("design:paramtypes", [])
], WebglComponent);
export { WebglComponent };
//# sourceMappingURL=webgl.component.js.map