import * as tslib_1 from "tslib";
import { Component, ViewChild, ElementRef } from '@angular/core';
const NUM_PARTICLES = 100;
const SIZE = 5;
let DemoComponent = class DemoComponent {
    constructor() {
        this.particles = [];
        this.rad = 1;
        this.t = 0;
        this.yAxis = 25;
        this.unit = 5;
        this.render = () => {
            this.ctx.clearRect(0, 0, this.w, this.h);
            for (var i = 0; i < NUM_PARTICLES; i++) {
                var p = this.particles[i];
                var x = this.t + (-this.yAxis + i) / this.unit;
                p.y += Math.sin(x);
                this.ctx.fillStyle = '#000';
                // tslint:disable-next-line: no-bitwise
                this.ctx.fillRect(~~(p.x + (this.w / 2 - (NUM_PARTICLES * SIZE / 2))), ~~(p.y + (this.h / 2 - 3)), SIZE, SIZE);
            }
            this.ctx.fillRect(~~(this.w / 2 - (NUM_PARTICLES * SIZE / 2)), ~~((this.h / 2 - 5)), 500, 5);
            this.t = this.t - (0.03 * Math.PI);
            requestAnimationFrame(this.render);
        };
    }
    ngOnInit() { }
    ngAfterViewInit() {
        this.drawParabol();
    }
    drawParabol() {
        var PARTICLE = {
            x: 0,
            y: 0
        };
        this.canvas = this.canvas_cover.nativeElement;
        this.ctx = this.canvas.getContext('2d');
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.w = this.canvas.width;
        this.h = this.canvas.height;
        this.particles = [];
        for (var i = 0; i < NUM_PARTICLES; i++) {
            var p = Object.create(PARTICLE);
            p.y = 0;
            p.x = (i + 1) * 5;
            p.vx = 0;
            p.vy = 0;
            p.dx = 0;
            p.dy = 0;
            p.rad = 1;
            this.particles.push(p);
        }
        this.render();
        console.log(this.particles);
    }
    getX(p) {
    }
};
tslib_1.__decorate([
    ViewChild('canvas_cover', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], DemoComponent.prototype, "canvas_cover", void 0);
tslib_1.__decorate([
    ViewChild('section_cover', { read: ElementRef, static: false }),
    tslib_1.__metadata("design:type", ElementRef)
], DemoComponent.prototype, "section_cover", void 0);
DemoComponent = tslib_1.__decorate([
    Component({
        selector: 'app-demo',
        templateUrl: './demo.component.html',
        styleUrls: ['./demo.component.scss'],
    }),
    tslib_1.__metadata("design:paramtypes", [])
], DemoComponent);
export { DemoComponent };
//# sourceMappingURL=demo.component.js.map