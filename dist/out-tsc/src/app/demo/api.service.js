import * as tslib_1 from "tslib";
import { of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType } from '@angular/common/http';
let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        this.apiUrl = "https://reqres.in";
        this.httpOptions = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
    }
    setToken() {
        this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + 'Token');
    }
    getUsers(page = 1) {
        return this.http.get(this.apiUrl + "/api/users?page=" + page, this.httpOptions).pipe(map(users => users), tap(users => { return users; }), catchError(this.handleError('getUsers', false)));
    }
    getUserNotFound() {
        return this.http.get(this.apiUrl + "/api/users/23", this.httpOptions).pipe(map(users => {
            return (users);
        }), catchError(this.handleError('getUserNotFound', false)));
    }
    getUsersProgress() {
        var req = new HttpRequest('GET', this.apiUrl + '/api/users', {
            reportProgess: true
        });
        return this.http.request(req).subscribe(event => {
            if (event.type === HttpEventType.Sent) {
                console.log(new Date().getTime());
                console.log(event);
            }
            if (event.type === HttpEventType.Response) {
                console.log(new Date().getTime());
                console.log(event);
            }
        });
    }
    handleError(operation = "operation", result) {
        return (error) => {
            alert(error.message);
            return of((result));
        };
    }
};
ApiService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [HttpClient])
], ApiService);
export { ApiService };
//# sourceMappingURL=api.service.js.map