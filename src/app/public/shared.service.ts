import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class SharedService {

	constructor() { }
	COMPANY_NAME = 'Amitgroup';
	COMPANY_ADDRESS = '181 Dinh Tien Hoang, DaKao Ward, District 1, 70000, Ho Chi Minh city, Vietnam';
	COMPANY_DESCRIPTION = 'AMIT Group provides information technology solutions and all-inclusive design services';
	COMPANY_EMAIL = 'contact@amitgroup.vn';
	COMPANY_PHONE = '(+84) 287 101 7979';
	COMPANY_LOGAN = 'Transform Digi Together';
	logo = {
		url: '../../../assets/images/logo.png',
		alt: 'Logo'
	};
	menu = [
		{
			name: 'Home',
			url: '/',
		},
		{
			name: 'Products',
			url: '/product',
		},
		{
			name: 'Service',
			url: '/service',
		},
		{
			name: 'Technology',
			url: '/technology'
		},
		{
			name: 'Blog',
			url: '/blog'
		},
		{
			name: 'Career',
			url: '/career'
		},
		{
			name: 'Company',
			url: '/company'
		},
		{
			name: 'Contact',
			url: '/contact'
		}
	];

	our_services = {
		header: 'Our Services',
		description: 'We offer web development, iOS and Android services together with all-inclusive design services. We have all the expertises you need to produce friendly, beautiful,stable, and scalable products.',
		services: [
			{
				name: 'iOS Development',
				icon: '../../../assets/images/Symbol.png',
				description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
			},
			{
				name: 'Android Development',
				icon: '../../../assets/images/Symbol.png',
				description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
			},
			{
				name: 'UX/UI Designer',
				icon: '../../../assets/images/Symbol.png',
				description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
			},
			{
				name: 'Web Development',
				icon: '../../../assets/images/Symbol.png',
				description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
			},
			{
				name: 'Game Development',
				icon: '../../../assets/images/Symbol.png',
				description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
			},
			{
				name: 'Embedded Software',
				icon: '../../../assets/images/Symbol.png',
				description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'
			},
		]
	};
	our_clients = {
		header: 'Our Clients Say',
		description: 'Our Client Description',
		clients: [
			{
				image: '../../../assets/images/Symbol.png',
				name: 'Client Name',
				position: 'Client Position',
				description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
			},
			{
				image: '../../../assets/images/Symbol.png',
				name: 'Client Name',
				position: 'Client Position',
				description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
			},
			{
				image: '../../../assets/images/Symbol.png',
				name: 'Client Name',
				position: 'Client Position',
				description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
			},
			{
				image: '../../../assets/images/Symbol.png',
				name: 'Client Name',
				position: 'Client Position',
				description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
			},
		]
	};
	modern_platforms = {
		header: 'Modern Platforms',
		description: 'A software is good when it can be run on many platforms. We are trying our best to response all our client\'s demand a wide range of services covers many platforms.',
		platforms: [
			{
				name: 'Name Platform',
				icon: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Name Platform',
				icon: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Name Platform',
				icon: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Name Platform',
				icon: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Name Platform',
				icon: '../../../assets/images/Symbol.png'
			},
		]
	};
	diverse_experiences = {
		header: 'We Have Diverse Experience',
		description: 'We offer web development, iOS and Android services together with all-inclusive design services. We have all the expertises you need to produce friendly, beautiful,stable, and scalable products.',
		experiences: [
			{
				name: 'Experience Name',
				image: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/Symbol.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/Symbol.png'
			},
		]
	};
	getMenu() {
		return of (this.menu);
	}
	getLogo() {
		return of(this.logo);
	}
}
