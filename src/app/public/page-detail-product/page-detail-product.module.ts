import { PageDetailProductComponent } from './page-detail-product.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PageDetailProductComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageDetailProductComponent
      }
    ])
  ]
})
export class PageDetailProductModule { }
