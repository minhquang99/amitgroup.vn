import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DemoService } from '../services/demo.service';
@Component({
  selector: 'app-page-detail-product',
  templateUrl: './page-detail-product.component.html',
  styleUrls: ['./page-detail-product.component.scss']
})
export class PageDetailProductComponent implements OnInit {
  product;
  constructor(private activatedRoute: ActivatedRoute, private demo: DemoService) { }

  ngOnInit() {
    var productSlug = this.activatedRoute.snapshot.paramMap.get('slug');
    this.demo.getProduct(productSlug).subscribe(p => { this.product = p; });
  }
}
