import { FullpageDirective } from './../directive/fullpage.directive';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { PartnerComponent } from './partner/partner.component';
import { DiverseExperienceComponent } from './diverse-experience/diverse-experience.component';
import { ModernPlatformsComponent } from './modern-platforms/modern-platforms.component';
import { CoverComponent } from './cover/cover.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurServicesComponent } from './our-services/our-services.component';
import { OurClientsComponent } from './our-clients/our-clients.component';

@NgModule({
  declarations: [
    HomeComponent,
    CoverComponent,
    OurServicesComponent,
    OurClientsComponent,
    ModernPlatformsComponent,
    DiverseExperienceComponent,
    PartnerComponent,
    FullpageDirective
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent
      }
    ])
  ]
})
export class HomeModule { }
