import { Component, OnInit, AfterViewInit } from '@angular/core';
import { DemoService } from '../../services/demo.service';
import Swiper from 'swiper';
@Component({
	selector: 'home-our-clients',
	templateUrl: './our-clients.component.html',
	styleUrls: ['./our-clients.component.scss']
})
export class OurClientsComponent implements OnInit, AfterViewInit {
	our_clients;
	constructor(private demo: DemoService) { }

	ngOnInit() {
		this.our_clients = this.demo.our_clients;
	}
	ngAfterViewInit() {
		var swiper = new Swiper('.swiper-clients', {
			slidesPerView: 2,
			spaceBetween: 30,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			breakpoints: {
				1024: {
					slidesPerView: 1,
				}
			}
		});
	}
}

