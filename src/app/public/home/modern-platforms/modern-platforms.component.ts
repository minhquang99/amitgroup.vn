import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { DemoService } from '../../services/demo.service';

import $ from 'jquery';
@Component({
	selector: 'home-modern-platforms',
	templateUrl: './modern-platforms.component.html',
	styleUrls: ['./modern-platforms.component.scss']
})
export class ModernPlatformsComponent implements OnInit, AfterViewInit {
	modern_platforms;
	currentWidth = 0;
	constructor(private demo: DemoService) { }

	ngOnInit() {
		this.modern_platforms = this.demo.modern_platforms;
	}
	ngAfterViewInit() {
		this.resizePlatform();
	}
	resizePlatform() {
		document.querySelectorAll('.section-platforms .platform').forEach((platform: HTMLElement) => {
			platform.style.height = platform.clientWidth + 'px';
		});
		this.currentWidth = window.innerWidth;
	}
	@HostListener('window:resize')
	onresize() {
		if (window.innerWidth != this.currentWidth) {
			this.resizePlatform();
		}
	}
}
