import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModernPlatformsComponent } from './modern-platforms.component';

describe('ModernPlatformsComponent', () => {
  let component: ModernPlatformsComponent;
  let fixture: ComponentFixture<ModernPlatformsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModernPlatformsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModernPlatformsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
