import { ParticlesService } from './../../services/particles.service';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { DemoService } from '../../services/demo.service';
@Component({
	selector: 'home-cover',
	templateUrl: './cover.component.html',
	styleUrls: ['./cover.component.scss']
})
export class CoverComponent implements OnInit, AfterViewInit {
	stop = false;
	@ViewChild('canvas_cover', { read: ElementRef, static: false }) canvas_cover: ElementRef;
	@ViewChild('section_cover', { read: ElementRef, static: false }) section_cover: ElementRef;
	@ViewChild('logo_full', { read: ElementRef, static: false }) logo_full: ElementRef;
	@ViewChild('cover_slider', { read: ElementRef, static: false }) cover_slider: ElementRef;
	listImages = [];
	constructor(public demo: DemoService, private router: Router, private particleService: ParticlesService) {
		this.router.events.subscribe((event) => {
			if (event instanceof NavigationEnd) {
				if (event.url != '/') {
					this.particleService.stop();
				}
			}
		});
	}
	ngOnInit() {
	}
	ngAfterViewInit() {
		this.particleService.startParticleSlider(this.canvas_cover.nativeElement, this.section_cover.nativeElement);
		var logo = this.logo_full.nativeElement;
		logo.style.top = (window.innerWidth > 1200) ? ((window.innerHeight) / 920) * 75 - 85 + 94 + 'px' : '50px';
		logo.style.height = (window.innerWidth > 1200) ? ((window.innerHeight - 94) / 920) * 190 + 'px' : '190px';
		logo.style.left = (window.innerWidth > 1200) ? (window.innerWidth / 1618) * 680 + 'px' : '50%';
	}
	slide(event) {
		var span = event.target as HTMLElement;
		if (!span.classList.contains('active')) {
			var id = span.getAttribute('data-slide-to');
			// tslint:disable-next-line: radix
			this.particleService.moveSlide(parseInt(id));
			document.querySelectorAll('.cover-slide').forEach((cs: HTMLElement) => {
				cs.classList.remove('active');
			});
			document.getElementById('cover-slide-' + id).classList.add('active');
			document.querySelectorAll('.cover-indicators .indicator').forEach((s: HTMLElement) => {
				s.classList.remove('active');
			});
			span.classList.add('active');
		}
	}
	@HostListener('window:resize')
	onresize() {
		this.logo_full.nativeElement.style.top = (window.innerWidth > 1200) ? ((window.innerHeight) / 920) * 75 - 85 + 94 + 'px' : '50px';
		this.logo_full.nativeElement.style.height = (window.innerWidth > 1200) ? ((window.innerHeight - 94) / 920) * 190 + 'px' : '190px';
		this.logo_full.nativeElement.style.left = (window.innerWidth > 1200) ? (window.innerWidth / 1600) * 680 + 'px' : '50%';
	}
	/** WebGL */
}
