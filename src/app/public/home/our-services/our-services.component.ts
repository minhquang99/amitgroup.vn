import { Component, OnInit, Directive, HostListener } from '@angular/core';

import { DemoService } from '../../services/demo.service';

@Directive({selector: '[ourservice]'})
class OurService {
	@HostListener('click', ['$event.target'])
	onClick(target) {
		console.log(target);
	}
}
@Component({
	selector: 'home-our-services',
	templateUrl: './our-services.component.html',
	styleUrls: ['./our-services.component.scss']
})
export class OurServicesComponent implements OnInit {
	our_services;

	constructor(private demo: DemoService) { }

	ngOnInit() {
		this.our_services = this.demo.our_services;
	}
}
