import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiverseExperienceComponent } from './diverse-experience.component';

describe('DiverseExperienceComponent', () => {
  let component: DiverseExperienceComponent;
  let fixture: ComponentFixture<DiverseExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiverseExperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiverseExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
