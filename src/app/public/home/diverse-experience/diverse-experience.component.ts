import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { DemoService } from '../../services/demo.service';
@Component({
	selector: 'home-diverse-experience',
	templateUrl: './diverse-experience.component.html',
	styleUrls: ['./diverse-experience.component.scss']
})
export class DiverseExperienceComponent implements OnInit, AfterViewInit {
	diverse_experiences;
	currentWidth = 0;
	constructor(private demo: DemoService) { }
	ngOnInit() {
		this.diverse_experiences = this.demo.diverse_experiences;
	}
	ngAfterViewInit() {
		this.resizeExp();
	}
	resizeExp() {
		document.querySelectorAll('.section-experiences .experience img').forEach((img: HTMLElement) => {
			img.onload = () => {
				var exp = img.parentElement;
				exp.style.height = exp.offsetWidth + 'px';
			}
		});
		this.currentWidth = window.innerWidth;
	}
	@HostListener('window:resize')
	onresize() {
		if (window.innerWidth != this.currentWidth) {
			this.resizeExp();
		}
	}
}
