
import { PartnerService } from './../../services/partner.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import Swiper from 'swiper';
@Component({
  selector: 'home-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit, AfterViewInit {
  partners = [];
  constructor(private partnerService: PartnerService) { }

  ngOnInit() {
    if (this.partnerService.partners.length <= 0) {
      this.partnerService.createDemoPartner().subscribe(pArr => {
        this.partners = pArr;
      });
    } else {
      this.partners = this.partnerService.partners;
    }
  }
  ngAfterViewInit(): void {
    var swiper = new Swiper('.swiper-partner', {
      slidesPerView: 6,
      spaceBetween: 1,
      freeMode: true,
      breakpoints: {
        1024: {
          slidesPerView: 3,
        },
        768: {
          slidesPerView: 2,
        }
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  }
}
