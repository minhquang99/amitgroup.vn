import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PageTechnologyComponent } from './page-technology.component';

@NgModule({
  declarations: [
    PageTechnologyComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageTechnologyComponent
      }
    ])
  ]
})
export class PageTechnologyModule { }
