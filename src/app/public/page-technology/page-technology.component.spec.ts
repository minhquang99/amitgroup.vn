import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTechnologyComponent } from './page-technology.component';

describe('PageTechnologyComponent', () => {
  let component: PageTechnologyComponent;
  let fixture: ComponentFixture<PageTechnologyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageTechnologyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTechnologyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
