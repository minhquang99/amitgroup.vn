import { DemoService } from './demo.service';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Product } from '../models/product';
@Injectable({
	providedIn: 'root'
})
export class ProductService {
	products: Array<Product> = [];

	mockCategory = [
		{
			id: 1,
			name: 'E-Commerce',
		},
		{
			id: 2,
			name: 'Education'
		},
		{
			id: 3,
			name: 'Healthcare'
		},
		{
			id: 4,
			name: 'Hospital',
		},
		{
			id: 5,
			name: 'Media & Entertainment',
		},
		{
			id: 6,
			name: 'Telecom & Networking'
		},
		{
			id: 7,
			name: 'Automotive Services & Dealerships'
		},
		{
			id: 8,
			name: 'Fintech & Commodity Exchange Trading'
		}
	];
	constructor(private demo: DemoService) { }
	createMockProducts(): Observable<Array<Product>> {
		if (this.products.length <= 0) {
			// tslint:disable-next-line: prefer-for-of
			for (let i = 0; i < this.demo.products.length; i++) {
				var product = this.demo.products[i];
				this.products.push(new Product({
					id: product.id,
					name: product.name,
					slug: product.slug,
					category: product.category,
					image: product.image
				}));
			}
			return of(this.products);
		} else {
			return of(this.products);
		}
	}
}
