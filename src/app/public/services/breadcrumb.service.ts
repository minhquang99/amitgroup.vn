import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, RouteConfigLoadEnd, ChildActivationEnd } from '@angular/router';
import { filter, map, distinctUntilChanged } from 'rxjs/operators';

import { BreadCrumb } from '../models/breadcrumb';
import { Observable, of } from 'rxjs';
@Injectable({
	providedIn: 'root'
})
export class BreadcrumbService {
	navigationEnd$ = this.router.events.pipe(
		filter(event => event instanceof NavigationEnd),
		map(event => this.buildBreadCrumb(this.activatedRoute.root))
	);
	constructor(private router: Router, private activatedRoute: ActivatedRoute, private BCService: BreadcrumbService) {
	}
	buildBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: Array<BreadCrumb> = []): Array<BreadCrumb> {
		if (route) {
			const label = route.routeConfig ? route.routeConfig.data['breadcrumb'] : 'Home';
			const path = route.routeConfig ? route.routeConfig.path : '';
			const nextUrl = `${url}${path}/`;
			const breadcrumb = {
				label: label,
				url: nextUrl
			};
			const newBreadCrumbs = [...breadcrumbs, breadcrumb];
			if (route.firstChild) {
				return this.buildBreadCrumb(route.firstChild.firstChild, nextUrl, newBreadCrumbs);
			}
			return newBreadCrumbs;
		} else {
			return breadcrumbs;
		}
	}
}
