import { DemoService } from './demo.service';
import { Partner } from './../models/partner';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PartnerService {
  partners: Array<Partner> = [];
  constructor(private demo: DemoService) { }
  createDemoPartner(): Observable<any> {
    this.demo.partners.forEach( partner => {
      var p = new Partner(partner);
      this.partners.push(p);
    });
    return of(this.partners);
  }
}
