import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class DemoService {

	constructor() { }
	COMPANY_NAME = 'Amitgroup';
	COMPANY_ADDRESS = '07 7C Street, An Phu An Khanh urban area, An Phu Ward, Thu Duc city (old District 2 ), Ho Chi Minh city';
	COMPANY_ADDRESS_2 = 'Ha Noi: 112 Tran Phu, Ha Dong, Ha Noi';
	COMPANY_DESCRIPTION = 'AMIT Group provides information technology solutions and all-inclusive design services';
	COMPANY_EMAIL = 'contact@amitgroup.vn';
	COMPANY_PHONE = '(+84) 287 101 7979';
	COMPANY_LOGAN = 'Transform Digi Together';
	COMPANY_MST = '0315510205';
	logo = {
		url: '../../../assets/images/logo.png',
		alt: 'Logo'
	};
	menu = [
		{
			name: 'Home',
			url: '/',
		},
		{
			name: 'Products',
			url: '/product',
		},
		{
			name: 'Service',
			url: '/service',
		},
		{
			name: 'Technology',
			url: '/technology'
		},
		{
			name: 'Blog',
			url: '/blog'
		},
		{
			name: 'Career',
			url: '/career'
		},
		{
			name: 'About Us',
			url: '/about-us'
		},
		{
			name: 'Contact',
			url: '/contact'
		}
	];

	our_services = {
		header: 'Our Services',
		description: 'We offer web development, iOS and Android services together with all-inclusive design services. We have all the expertises you need to produce friendly, beautiful,stable, and scalable products.',
		services: [
			{
				name: 'iOS Development',
				image: '../../../assets/images/services/ios.png',
				image_2: '../../../assets/images/services/ios_2.png',
				description: 'We use modern programming languages, such as Swift and Objective-C, and proven technologies and approaches that allow us to easily extend and scale our products. '
			},
			{
				name: 'Android Development',
				image: '../../../assets/images/services/android.png',
				image_2: '../../../assets/images/services/android.png',
				description: 'We develop Android apps in Java and Kotlin. Given the broad range of devices capable of running Android, we can create apps for everything from smartwatches to smart TVs. '
			},
			{
				name: 'Website Development',
				image: '../../../assets/images/services/web.png',
				image_2: '../../../assets/images/services/web.png',
				description: 'We provide both frontend and backend development services. Our web development team specializes in Ruby on Rails, Go, JavaScript, AngularJS, React, Node.js, and Elixir. '
			},
			{
				name: 'UX/UI Designer',
				image: '../../../assets/images/services/uxui.png',
				image_2: '../../../assets/images/services/uxui.png',
				description: 'We offer UX/UI design, brand identity design, prototyping for web and mobile products. We focus how people will use our clients’ products. '
			},
			{
				name: 'Game Development',
				image: '../../../assets/images/services/game.png',
				image_2: '../../../assets/images/services/game.png',
				description: 'All the things we do included application gamification that is the new helps your business get the best improvement in user experience '
			},
			{
				name: 'Embedded Software & AI',
				image: '../../../assets/images/services/embedded.png',
				image_2: '../../../assets/images/services/embedded.png',
				description: 'We supply services as embedded software for device of car, in-house, office, and smart factory; AI intelligent processing software as face detection, image detection, camera detection '
			},
		]
	};
	our_clients = {
		header: 'Our Clients Say',
		description: 'Our Client Description',
		clients: [
			{
				image: '../../../assets/images/home/useacc.jpg',
				name: 'Client Name',
				position: 'Client Position',
				description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
			},
			{
				image: '../../../assets/images/home/useacc.jpg',
				name: 'Client Name',
				position: 'Client Position',
				description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
			},
			{
				image: '../../../assets/images/home/useacc.jpg',
				name: 'Client Name',
				position: 'Client Position',
				description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
			},
			{
				image: '../../../assets/images/home/useacc.jpg',
				name: 'Client Name',
				position: 'Client Position',
				description: 'There are many variation of passages of lorem et vailable, but the majority have suffered in loremst massa words look Aenean congue mi sit amet arcu aliquet'
			},
		]
	};
	modern_platforms = {
		header: 'Modern Platforms',
		description: 'A software is good when it can be run on many platforms. We are trying our best to response all our client\'s demand a wide range of services covers many platforms.',
		platforms: [
			{
				name: 'Web',
				icon: '../../../assets/images/platforms/web.png',
				icon_2: '../../../assets/images/platforms/web.png',
			},
			{
				name: 'iOS',
				icon: '../../../assets/images/platforms/ios.png',
				icon_2: '../../../assets/images/platforms/ios_2.png'
			},
			{
				name: 'Android',
				icon: '../../../assets/images/platforms/android.png',
				icon_2: '../../../assets/images/platforms/android.png'
			},
			{
				name: 'IoT',
				icon: '../../../assets/images/platforms/iot.png',
				icon_2: '../../../assets/images/platforms/iot.png'
			},
			{
				name: 'AI',
				icon: '../../../assets/images/platforms/ai.png',
				icon_2: '../../../assets/images/platforms/ai.png'
			},
		]
	};
	diverse_experiences = {
		header: 'We Have Diverse Experience',
		description: 'We offer web development, iOS and Android services together with all-inclusive design services. We have all the expertises you need to produce friendly, beautiful,stable, and scalable products.',
		experiences: [
			{
				name: 'Experience Name',
				image: '../../../assets/images/experience/1.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/experience/2.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/experience/3.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/experience/4.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/experience/5.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/experience/6.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/experience/7.png'
			},
			{
				name: 'Experience Name',
				image: '../../../assets/images/experience/8.png'
			},
		]
	};
	products = [
		{
			id: 0,
			name: 'Head-Up Screen',
			slug: 'head-up-screen',
			category: 6,
			image: '../../../assets/images/product/head-up_screen.png',
			detail_image: '../../../assets/images/product/product-detail/head-up_screen.png',
			requirement: 'The Head-Up Screen app allows you to set the settings of the installed Head-Up Screens Accessory product through wireless network. The Head-Up Screen is a new and innovative product, which displays different vehicle and navigation information on a transparent screen.',
			client: '../../../assets/images/product-client/bmw.png'
		},
		{
			id: 1,
			name: 'Universal Traffic Recorder',
			slug: 'universal-traffic-recorder',
			category: 7,
			image: '../../../assets/images/product/universal_traffic_recorder.png',
			detail_image: '../../../assets/images/product/product-detail/universal_traffic_recorder.png',
			requirement: 'Universal Traffic Recorder (UTR), the application connects to the camera device via wifi Socket service to set parameters for the device images of 2 front and rear cameras are displayed. It also supports a lot for parking feature, locating the car. The application also helps to return video if there are problems such as collisions via sensors on the device.',
			client: '../../../assets/images/product-client/audi.png'
		},
		{
			id: 2,
			name: 'Tang Thu Vien',
			slug: 'tang-thu-vien',
			category: 5,
			image: '../../../assets/images/product/tangthuvien.png',
			detail_image: '../../../assets/images/product/product-detail/tangthuvien.png',
			requirement: 'Application to build a community for swordplay enthusiasts based on the original forum which has been active for more than 10 years.. This application not only brings good reading experiences but also works as a social network for readers.',
			client: '../../../assets/images/product-client/tangthuvien.png'
		},
		{
			id: 3,
			name: 'Go Application',
			slug: 'go-application',
			category: 8,
			image: '../../../assets/images/product/go_application.png',
			detail_image: '../../../assets/images/product/product-detail/go_application.png',
			requirement: 'GXO is a support application for users of basic transportation services, buying and helping with small, handy products that you need to use everyday by motorcycle taxi, motorbike.',
			client: '../../../assets/images/product-client/go.png'
		},
		{
			id: 4,
			name: 'Block Puzzle Jewel 2019',
			slug: 'block-puzzle-jewel-2019',
			category: 5,
			image: '../../../assets/images/product/block_puzzle_jewel.png',
			detail_image: '../../../assets/images/product/product-detail/block_puzzle_jewel.png',
			requirement: 'LockBlock Puzzle Jewel 2019 is a free jigsaw puzzle. You can enjoy it everywhere, every time without being online.',
			client: '../../../assets/images/product-client/puzzle.png'
		},
		{
			id: 5,
			name: 'Smart Farm',
			slug: 'smart-farm',
			category: 5,
			image: '../../../assets/images/product/smart_farm.png',
			detail_image: '../../../assets/images/product/product-detail/smart_farm.png',
			requirement: 'High-tech Agricultural model designed for actual conditions in Vietnam. A system including environmental sensors and controllers. Collect information about temperature, air humidity, humidity of soil and light. A caring system for plant via Israeli technology. Automatic watering, ventilation, environmental adjustment to match the needs of the crop. Users can observe, control all devices via application on their phone.',
			client: '../../../assets/images/product-client/hifarm.png'
		},
		{
			id: 6,
			name: 'cBMotion',
			slug: 'cbmotion',
			category: 4,
			image: '../../../assets/images/product/cbmotion.png',
			detail_image: '../../../assets/images/product/product-detail/cbmotion.png',
			requirement: '',
			client: '../../../assets/images/product-client/bitis.png'
		},
		{
			id: 7,
			name: 'Hatoxu',
			slug: 'hatoxu',
			category: 1,
			image: '../../../assets/images/product/hatoxu.png',
			detail_image: '../../../assets/images/product/product-detail/hatoxu.png',
			requirement: 'E-commerce ecosystem that is integrated between website and app helps to optimize user experience, support sellers and buyers in uploading goods information, exchanging and paying.',
			client: '../../../assets/images/product-client/hatoxu.png'
		},
		{
			id: 8,
			name: 'Logistics Eco System',
			slug: 'logistics-eco-system',
			category: 7,
			image: '../../../assets/images/product/logistics.png',
			detail_image: '../../../assets/images/product/product-detail/logistics.png',
			requirement: 'The ecosystem of Logistics software for 3PL with more than 15 sub-software such as WMS, TMS, ONP, HR, Online Report, … is integrated into a unified system.',
			client: '../../../assets/images/product-client/logistics.png'
		},
		{
			id: 9,
			name: 'Verveba Telecom',
			slug: 'verveba-telecom',
			category: 6,
			image: '../../../assets/images/product/verveba_telecom.png',
			detail_image: '../../../assets/images/product/product-detail/verveba_telecom.png',
			requirement: 'The main portal that provides information to telecommunications providers.',
			client: '../../../assets/images/product-client/verveba.png'
		},
		{
			id: 10,
			name: 'Global Viet Nam Aupair',
			slug: 'global-vietname-aupair',
			category: 7,
			image: '../../../assets/images/product/global_vietnam_aupair.png',
			detail_image: '../../../assets/images/product/product-detail/global_vietnam_aupair.png',
			requirement: 'Multilingual website synchronized Trello for project management and information and supporting SEO.',
			client: '../../../assets/images/product-client/global.png'
		},
		// {
		// 	id: 11,
		// 	name: 'Topica',
		// 	category: 6,
		// 	image: '../../../assets/images/product/topica.png',
		// 	detail_image: '../../../assets/images/product/product-detail/topica.png',
		// 	requirement: '',
		// 	client: '../../../assets/images/product-client/bmw.png'
		// },
	];
	partners = [
		{
			name: 'Name',
			image: '../../../assets/images/partner/dhl_1.png'
		},
		{
			name: 'Name',
			image: '../../../assets/images/partner/bitis_1.png'
		},
		{
			name: 'Name',
			image: '../../../assets/images/partner/mobis_1.png'
		},
		{
			name: 'Name',
			image: '../../../assets/images/partner/klaussner_1.png'
		},
		{
			name: 'Name',
			image: '../../../assets/images/partner/sk_1.png'
		},
		{
			name: 'Name',
			image: '../../../assets/images/partner/logistics_1.png'
		},
	];
	getMenu() {
		return of(this.menu);
	}
	getLogo() {
		return of(this.logo);
	}
	getProduct(slug): Observable<any> {
		var p = this.products.filter( product => product.slug == slug );
		return of(p[0]);
	}
}
