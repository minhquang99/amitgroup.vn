import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { MessageComponent } from '../layout/message/message.component';
import { MatDialogRef, MatDialog } from '@angular/material';
@Injectable({ 
	providedIn: 'root'
})
export class MessageService {
	dialogRef: MatDialogRef<MessageComponent>;
	constructor( private dialog: MatDialog) { }

	public openDialog(title: string, message: string): Observable<any> {
		this.dialogRef = this.dialog.open(MessageComponent, { width: '300px'});
		this.dialogRef.componentInstance.title = title;
		this.dialogRef.componentInstance.message = message;

		return this.dialogRef.afterClosed();
	}
}
