import { TestBed } from '@angular/core/testing';

import { ParticlesService } from './particles.service';

describe('ParticlesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParticlesService = TestBed.get(ParticlesService);
    expect(service).toBeTruthy();
  });
});
