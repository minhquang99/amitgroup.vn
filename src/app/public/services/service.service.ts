import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

import { DemoService } from './demo.service';
import { Service } from '../models/service';
@Injectable({
	providedIn: 'root'
})
export class ServiceService {
	services: Array<Service> = [];
	constructor(private demo: DemoService) { }

	createDemoServices(): Observable<Array<Service>> {
		if (this.services.length <= 0){
			this.demo.our_services.services.forEach( (service, index) => {
				service["id"] = index;
				this.services.push(new Service(service));
			});
			return of(this.services);
		} else {
			return of(this.services);
		}
	}
}
