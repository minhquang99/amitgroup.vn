import { Observable, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message.service';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl = window.location.origin + '/api/';
  httpOptions = {
    // headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
  };
  // setToken() {
  //   this.httpOptions.headers = this.httpOptions.headers.set('Authorization', 'Bearer ' + 'Token');
  // }
  constructor(private http: HttpClient, private messageService: MessageService) { }

  sendMail(url, data): Observable<any> {
    return this.http.post(this.apiUrl + url, data, this.httpOptions).pipe(
      map((result: any) => {
        return (result);
      }),
      catchError(this.handleError('Error', false))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.messageService.openDialog('Error Occured', error.status + '\n' + error.error.message);
      return of(result as T);
    };
  }
}
