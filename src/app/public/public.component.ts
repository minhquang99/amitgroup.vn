import { ParticlesService } from './services/particles.service';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, HostListener, AfterContentInit } from '@angular/core';
import { Router, RouterOutlet, NavigationEnd, ActivatedRoute } from '@angular/router';
import { RouterAnimation } from './models/router-animation';
@Component({
	selector: 'app-public',
	templateUrl: './public.component.html',
	styleUrls: ['./public.component.scss'],
	animations: [
		RouterAnimation
	]
})
export class PublicComponent implements OnInit, AfterViewInit {
	constructor(private router: Router, private particleSv: ParticlesService) {
		this.particleSv.initImageData(this.listImages).then( () => {
			this.loaded = true;
		});
		this.router.events.subscribe((evt) => {
			if ((evt instanceof NavigationEnd)) {
				window.scrollTo(0, 0);
			}
		});
	}
	listImages = [
		{
			src: 'assets/images/hand.png',
			drawPosition: 'bottom',
			random: 'false'
		},
		{
			src: 'assets/images/head.png',
			drawPosition: 'center',
			random: 'true'
		},
	];
	loaded = false;
	@ViewChild('progressValue', { read: ElementRef, static: false }) progressValue: ElementRef;
	@ViewChild('progressBar', { read: ElementRef, static: false }) progressBar: ElementRef;

	sections = [];
	isScrolling = false;
	DELAY: number = 1500;
	current: number = 0;
	navbarFixed: boolean = false;

	ngOnInit() {
	}
	ngAfterViewInit() {
		window.scrollTo(0, 0);
	}

	prepareRoute() { }
}
