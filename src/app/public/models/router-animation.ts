import { trigger, state, style, transition, animate, query, group, animateChild } from '@angular/animations';
export const RouterAnimation =
	trigger('routeAnimations', [
		transition('* => Home', [
			style({ position: 'relative' }),
			query(':enter, :leave', [
				style({
					position: 'absolute',
					top: 0,
					left: 0,
					width: '100%',
					transform: 'translateX(0%)'
				}),
			]),
			query(':enter', [
				style({
					transform: 'translateX(-100%)'
				}),
			], { optional: true }),
			query(':leave', animateChild(), { optional: true }),
			group([
				query(':leave', [
					animate('800ms ease-out', style({ transform: 'translateX(100%)' }))
				], { optional: true }),
				query(':enter', [
					animate('800ms ease-out', style({ transform: 'translateX(0%)' }))
				])
			]),
			query(':enter', animateChild()),
		]),
		transition('* <=> *', [
			style({ position: 'relative' }),
			query(':enter, :leave', [
				style({
					position: 'absolute',
					top: 0,
					left: 0,
					width: '100%',
					transform: 'translateX(0%)'
				}),
			]),
			query(':enter', [
				style({
					transform: 'translateX(100%)'
				}),
			], { optional: true }),
			query(':leave', animateChild(), { optional: true }),
			group([
				query(':leave', [
					animate('800ms ease-out', style({ transform: 'translateX(-100%)' }))
				], { optional: true }),
				query(':enter', [
					animate('800ms ease-out', style({ transform: 'translateX(0%)' }))
				])
			]),
			query(':enter', animateChild()),
		]),
		// transition( 'Product => Home', [
		// 	style( { position: 'relative' }),
		// 	query(':enter, :leave', [
		// 		style({
		// 			position: 'absolute',
		// 			top: 0,
		// 			left: 0,
		// 			width: '100%'
		// 		}),
		// 		]),
		// 	query(':enter', [
		// 		style({
		// 			left: '100%'
		// 		}),
		// 		]),
		// 	query(':leave', animateChild()),
		// 	group([
		// 		query(':leave', [
		// 			animate('1000ms ease-out', style({ left: '-100%'}))
		// 			]),
		// 		query(':enter', [
		// 			animate('1000ms ease-out', style({ left: '0%'}))
		// 			])
		// 		]),
		// 	query(':enter', animateChild()),
		// ]),
	]);