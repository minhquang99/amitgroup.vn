export class Product {
	id: number;
	name: string;
	slug: string;
	category: number;
	image: string;
	constructor(product) {
		this.id = product.id;
		this.name = product.name;
		this.category = product.category;
		this.image = product.image;
		this.slug = product.slug;
	}
}