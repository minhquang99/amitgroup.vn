export class SectionFullpage {
    section: HTMLElement;
    constructor(element: HTMLElement) {
        this.section = element;
    };
    getOffsetTop() {
        return this.section.offsetTop;
    }
}