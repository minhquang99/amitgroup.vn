export class Service {
	id: number;
	name: string;
	image: string;
	description: string;
	image_2: string;
	constructor(service) {
		this.id = service.id;
		this.name = service.name;
		this.description = service.description;
		this.image = service.image;
		this.image_2 = service.image_2;
	}
}