export class Particles {
    // id: number;
    origin;
    img;
    imgWidth;
    imgHeight;
    imgData;
    drawPosition;
    canvas;
    left;
    top;
    width;
    height;
    translateX;
    translateY;
    originImg;
    // top: number;
    // left: number;
    // translateX;
    // translateY;
    particles = [];
    constructor() {
    }
    initImageData(img, canvas) {
        var particle = {
            x: 0,
            y: 0,
            vx: 0,
            vy: 0
        };
        return new Promise((resolve, reject) => {
            var image = new Image();
            image.onload = () => {

                this.originImg = image;
                // var random = image.getAttribute('data-random');
                var random = img.random;
                // this.drawPosition = image.getAttribute('data-position');
                this.drawPosition = img.drawPosition;
                this.imgHeight = this.getHeight(image.height);
                this.imgWidth = this.getWidth(image.width);
                this.canvas = canvas;
                var ctx = this.canvas.getContext('2d');
                canvas.height = window.innerHeight;
                canvas.width = window.innerWidth;
                ctx.clearRect(0, 0, canvas.height, canvas.width);
                ctx.drawImage(image, 0, 0, this.imgWidth, this.imgHeight);
                this.img = ctx.getImageData(0, 0, this.imgWidth, this.imgHeight);
                this.imgData = this.img.data;
                if (random == 'true') {
                    var obj = { 0: [], 1: [], 2: [], 3: [] };
                    for (let x = 0; x < this.img.width; x += 1) {
                        for (let y = 0; y < this.img.height; y += 1) {
                            var p = (x * 4 * this.img.width + y * 4);
                            if (this.imgData[(x * 4 + y * 4 * this.img.width) + 3] > 128 && Math.random() * 10 > 5) {
                                var i = weightedRandomDistrib((p / this.img.data.length) * 4);
                                var par = Object.create(particle);
                                par.ox = x;
                                par.oy = y;
                                par.x = window.innerWidth * Math.cos(Math.random() * Math.PI);
                                par.y = window.innerHeight * Math.cos(Math.random() * Math.PI);
                                par.accX = 0;
                                par.accY = 0;
                                par.vx = (Math.random() - 0.5) * 20;
                                par.vy = (Math.random() - 0.5) * 20;
                                par.friction = Math.random() * 0.001 + 0.94;
                                obj[i].push(par);
                            }
                        }
                    }
                    this.particles = (obj[3]);
                } else {
                    for (let x = 0; x < this.img.width; x += 1) {
                        for (let y = 0; y < this.img.height; y += 1) {
                            var p = (x * 4 + y * 4 * this.img.width);
                            if (this.imgData[p + 3] > 128) {
                                var par = Object.create(particle);
                                par.ox = x;
                                par.oy = y;
                                par.x = window.innerWidth * Math.cos(Math.random() * Math.PI);
                                par.y = window.innerHeight * Math.cos(Math.random() * Math.PI);
                                par.accX = 0;
                                par.accY = 0;
                                par.vx = (Math.random() - 0.5) * 20;
                                par.vy = (Math.random() - 0.5) * 20;
                                par.friction = Math.random() * 0.001 + 0.94;
                                this.particles.push(par);
                            }
                        }
                    }
                }
                this.setPosition();
                resolve();
            };
            image.src = img.src;
        });
    }
    reSize() {
        var particle = {
            x: 0,
            y: 0,
            vx: 0,
            vy: 0
        };
        return new Promise((resolve, reject) => {

            this.imgHeight = this.getHeight(this.originImg.height);
            this.imgWidth = this.getWidth(this.originImg.width);
            var random = this.originImg.getAttribute('data-random');
            var ctx = this.canvas.getContext('2d');
            this.canvas.height = window.innerHeight;
            this.canvas.width = window.innerWidth;
            ctx.clearRect(0, 0, this.canvas.height, this.canvas.width);
            ctx.drawImage(this.originImg, 0, 0, this.imgWidth, this.imgHeight);
            this.img = ctx.getImageData(0, 0, this.imgWidth, this.imgHeight);
            this.imgData = this.img.data;

            if (random == 'true') {
                var obj = { 0: [], 1: [], 2: [], 3: [] };
                for (let x = 0; x < this.img.width; x++) {
                    for (let y = 0; y < this.img.height; y++) {
                        var p = (x * 4 * this.img.width + y * 4);
                        if (this.imgData[(x * 4 + y * 4 * this.img.width) + 3] > 128) {
                            var i = weightedRandomDistrib((p / this.img.data.length) * 4);
                            var par = Object.create(particle);
                            par.ox = x;
                            par.oy = y;
                            par.x = window.innerWidth * Math.cos(Math.random() * Math.PI);
                            par.y = window.innerHeight * Math.cos(Math.random() * Math.PI);
                            par.accX = 0;
                            par.accY = 0;
                            par.vx = (Math.random() - 0.5) * 20;
                            par.vy = (Math.random() - 0.5) * 20;
                            par.friction = Math.random() * 0.025 + 0.94;
                            obj[i].push(par);
                        }
                    }
                }
                this.particles = (obj[3]);
            } else {
                for (let x = 0; x < this.img.width; x++) {
                    for (let y = 0; y < this.img.height; y++) {
                        var p = (x * 4 + y * 4 * this.img.width);
                        if (this.imgData[p + 3] > 128) {
                            var par = Object.create(particle);
                            par.ox = x;
                            par.oy = y;
                            par.x = window.innerWidth * Math.cos(Math.random() * Math.PI);
                            par.y = window.innerHeight * Math.cos(Math.random() * Math.PI);
                            par.accX = 0;
                            par.accY = 0;
                            par.vx = (Math.random() - 0.5) * 20;
                            par.vy = (Math.random() - 0.5) * 20;
                            par.friction = Math.random() * 0.025 + 0.94;
                            this.particles.push(par);
                        }
                    }
                }
            }

            resolve();
        });
    }
    setPosition() {
        var t = 0;
        var l = 0;
        this.canvas.height = window.innerHeight;
        this.canvas.width = window.innerWidth;
        this.top = this.getTop();
        this.left = this.getLeft();
        this.translateX = this.getTranslateX();
        this.translateY = this.getTranslateY();
        this.particles.map(p => {
            p.x = window.innerWidth * Math.cos(Math.random() * Math.PI);
            p.y = window.innerHeight * Math.cos(Math.random() * Math.PI);
        });
    }
    getTop() {
        var t = 0;
        var h = this.canvas.height;
        var mh = this.imgHeight;
        if (this.drawPosition == 'center') {
            t = (h / 2 - mh / 2);
        }
        if (this.drawPosition == 'bottom') {
            t = (window.innerWidth > 1200) ? (h - mh) : (h - mh - 200);
        }
        return t;
    }
    getLeft() {
        var l = 0;
        var w = this.canvas.width;
        var mw = this.imgWidth;
        if (this.drawPosition == 'center') {
            l = (window.innerWidth > 1200) ? w / 2 : (w / 2 - mw / 2);
        }
        if (this.drawPosition == 'bottom') {
            l = (w - mw);
        }
        return l;
    }
    getWidth(w) {
        var maxw = (window.innerWidth > 1024) ? 550 : 300;
        var width = 0;
        if (this.drawPosition == 'center') {
            if (window.innerWidth <= 768) {
                width = window.innerWidth - 20;
            }
            if (window.innerWidth > 768) {
                width = maxw;
            }
            if (window.innerWidth > 1200) {
                var rate = (window.innerWidth / 1618);
                width = w * rate;
            }
            if (width > maxw) { width = maxw; }
        }
        if (this.drawPosition == 'bottom') {
            var rate = (window.innerWidth / 1618);
            var width = 0;
            if (window.innerWidth <= 768) {
                width = window.innerWidth;
            }
            if (window.innerWidth > 768) {
                width = window.innerWidth;
            }
            if (window.innerWidth > 1200) {
                width = w * rate;
            }
        }
        return width;
    }
    getHeight(h) {
        var height = 0;
        if (this.drawPosition == 'center') {
            var maxh = (window.innerWidth > 1024) ? 600 : 300;
            var rate = ((window.innerHeight - 94) / 920);
            if (window.innerWidth <= 768) {
                height = (window.innerWidth - 20) * (970 / 920);
            }
            if (window.innerWidth > 768) {
                height = h * rate;
            }
            if (window.innerWidth > 1200) {
                height = h * rate;
            }
            if (height > maxh) { height = maxh; }
        }
        if (this.drawPosition == 'bottom') {
            var maxh = window.innerHeight;
            var rate = ((window.innerHeight - 64) / 920);
            if (window.innerWidth <= 768) {
                height = (window.innerWidth * (970 / 920));
            }
            if (window.innerWidth > 768) {
                if (window.innerWidth < window.innerHeight) {
                    height = (window.innerWidth * (970 / 920));
                } else {
                    height = (window.innerHeight * (970 / 920));
                }
            }
            if (window.innerWidth > 1200) {
                height = h * rate;
            }
        }
        return height;
    }
    getTranslateX() {
        var x = 0;
        if (this.drawPosition == 'center') {
            x = 0;
        }
        if (this.drawPosition == 'bottom') {
            x = (window.innerWidth > 1200) ? 0 : window.innerWidth / 3;
        }
        return x;
    }
    getTranslateY() {
        var y = 0;
        if (this.drawPosition == 'center') {
            y = 47;
        }
        if (this.drawPosition == 'bottom') {
            if (window.innerWidth > 1200) {
                y = 0;
            }
        }
        return y;
    }
}
function weightedRandomDistrib(peak) {
    var prob = []; var seq = [];
    for (let i = 0; i < 4; i++) {
        prob.push(Math.pow(4 - Math.abs(peak - i), 3));
        seq.push(i);
    }
    var arr = [];
    for (let i = 0; i < prob.length; i++) {
        var n = Math.floor(prob[i]);
        for (let j = 0; j < n; j++) {
            arr.push(seq[i]);
        }
    }
    return arr[Math.floor(Math.random() * arr.length)];
}

