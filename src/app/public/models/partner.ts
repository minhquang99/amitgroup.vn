export class Partner {
    name: string;
    image: string;
    constructor(object) {
        this.name = object.name;
        this.image = object.image;
    }
}