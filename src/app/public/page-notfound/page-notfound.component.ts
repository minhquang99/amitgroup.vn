import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
@Component({
	selector: 'app-page-notfound',
	templateUrl: './page-notfound.component.html',
	styleUrls: ['./page-notfound.component.scss']
})
export class PageNotfoundComponent implements OnInit, AfterViewInit {
	@ViewChild('particles_field_1', { read: ElementRef, static: false }) particles_field_1: ElementRef;
	@ViewChild('particles_field_2', { read: ElementRef, static: false }) particles_field_2: ElementRef;
	imageUrl = '../../../assets/images/404/404.png';
	lineUrl = '../../../assets/images/404/lines.png';
	logo = '../../../assets/images/Symbol.png';
	particles1 = [];
	particles2 = [];
	width = 422;
	height = 222;
	size = 10;
	color = '#016E7D';
	constructor(private location: Location, private titleService: Title) {
		this.titleService.setTitle('Page Not Found - Amitgroup - Transform Digi Together');
	}

	ngOnInit() {
	}
	// tslint:disable-next-line: use-lifecycle-interface
	ngAfterViewInit() {
		this.particles1 = this.createParticle(this.particles1);
		this.particles2 = this.createParticle(this.particles2);
		this.drawCanvas(this.particles_field_1.nativeElement, this.particles1);
		this.drawCanvas(this.particles_field_2.nativeElement, this.particles2);
	}
	backPrevious() {
		this.location.back();
	}
	drawCanvas(canvas, particles) {
		var ctx = canvas.getContext('2d');
		requestAnimationFrame(render);
		function render() {
			ctx.clearRect(0, 0, 422, 222);
			ctx.save();
			ctx.fillStyle = '#016E7D';
			// tslint:disable-next-line: prefer-for-of
			for (let i = 0; i < particles.length; i++) {
				var particle = particles[i];
				if (particle.x + 20 > 422) { particle.move_x = (-1 * Math.random()); }
				if (particle.x <= 0) { particle.move_x = (1 * Math.random()); }
				if (particle.y + 20 > 222) { particle.move_y = (-1 * Math.random()); }
				if (particle.y <= 0) { particle.move_y = (1 * Math.random()); }
				particle.x = particle.x + particle.move_x * 5;
				particle.y = particle.y + particle.move_y * 5;
				ctx.beginPath();
				ctx.arc(particle.x, particle.y, 20, 0, Math.PI * 2, true);
				ctx.fill();
			}
			ctx.restore();
			requestAnimationFrame(render);
		}
	}

	createParticle(arr) {
		for (let i = 0; i < 7; i++) {
			arr[i] = {
				x: Math.random() * this.width,
				y: Math.random() * this.height,
				move_x: Math.random(),
				move_y: Math.random()
			};
		}
		return arr;
	}
}
