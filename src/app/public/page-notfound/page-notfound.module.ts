import { PageNotfoundComponent } from './page-notfound.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PageNotfoundComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageNotfoundComponent
      }
    ])
  ]
})
export class PageNotfoundModule { }
