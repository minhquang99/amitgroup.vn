import { PageCompanyComponent } from './page-company.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PageCompanyComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageCompanyComponent
      }
    ])
  ]
})
export class PageCompanyModule { }
