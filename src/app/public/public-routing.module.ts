import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './public.component';
const publicRoute: Routes = [
  {
		path: '',
		component: PublicComponent,
		data: {
			breadcrumb: 'Home'
		},
		children: [
			{
				path: '',
				loadChildren: () => import('./home/home.module').then(mod => mod.HomeModule),
				data: {
					breadcrumb: 'Home',
					animation: 'Home'
				}
			},
			{
				path: 'product',
				loadChildren: () => import('./page-product/page-product.module').then(mod => mod.PageProductModule),
				data: {
					breadcrumb: 'Product',
					animation: 'Product'
				}
			},
			{
				path: 'product/:slug',
				loadChildren: () => import('./page-detail-product/page-detail-product.module').then(mod => mod.PageDetailProductModule),
				data: {
					breadcrumb: 'Product Detail',
					animation: 'Product Detail'
				}
			},
			{
				path: 'service',
				loadChildren: () => import('./page-service/page-service.module').then(mod => mod.PageServiceModule),
				data: {
					breadcrumb: 'Service',
					animation: 'Service'
				}
			},
			{
				path: 'technology',
				loadChildren: () => import('./page-technology/page-technology.module').then(mod => mod.PageTechnologyModule),
				data: {
					breadcrumb: 'Technology',
					animation: 'Technology'
				}

			},
			{
				path: 'blog',
				loadChildren: () => import('./page-blog/page-blog.module').then(mod => mod.PageBlogModule),
				data: {
					breadcrumb: 'Blog',
					animation: 'Blog'
				}

			},
			{
				path: 'career',
				loadChildren: () => import('./page-career/page-career.module').then(mod => mod.PageCareerModule),
				data: {
					breadcrumb: 'Career',
					animation: 'Career'
				}

			},
			{
				path: 'contact',
				loadChildren: () => import('./page-contact/page-contact.module').then(mod => mod.PageContactModule),
				data: {
					breadcrumb: 'Contact',
					animation: 'Contact'
				}
			},
			{
				path: 'about-us',
				loadChildren: () => import('./page-company/page-company.module').then(mod => mod.PageCompanyModule),
				data: {
					breadcrumb: 'About Us',
					animation: 'About Us'
				}
			},

		]
	},
	{
		path: 'maintenance',
		loadChildren: () => import('./page-maintenance/page-maintenance.module').then(mod => mod.PageMaintenanceModule),
		data: {
			breadcrumb: 'Maintenance',
			animation: 'Maintenance'
		}
	},
	{
		path: '**',
		loadChildren: () => import('./page-notfound/page-notfound.module').then(mod => mod.PageNotfoundModule),
		data: {
			breadcrumb: '404',
			animation: '404'
		}
	},
];
@NgModule({
  imports: [RouterModule.forChild(publicRoute)],
	exports: [RouterModule]
})
export class PublicRoutingModule { }
