import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-career',
  templateUrl: './page-career.component.html',
  styleUrls: ['./page-career.component.scss']
})
export class PageCareerComponent implements OnInit {
  configUrl = 'https://amitgroup.api.demo-amit.com/api/v1/careers';
  listCareer: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getCareer();
  }

  getCareer() {
    return this.http.get<any>(this.configUrl).subscribe((res: any) => {
      console.log(res.data.results)
      this.listCareer = res.data.results;
    });
  }

}
