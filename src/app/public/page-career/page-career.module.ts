import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageCareerComponent } from './page-career.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    PageCareerComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageCareerComponent
      }
    ])
  ]
})
export class PageCareerModule { }
