import { style, query } from '@angular/animations';
import { Directive, ElementRef, AfterContentInit, AfterViewInit, HostListener, ViewChild } from '@angular/core';
import { NavigationEnd } from '@angular/router';
@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[fullpage]'
})
export class FullpageDirective implements AfterViewInit {
  isMobile = false;
  constructor(el: ElementRef) {
    this.parent = el.nativeElement;
    if (window.innerWidth > 1024 && this.Enable) {
      this.parent.style.height = '100vh';
      this.parent.style.overflow = 'hidden';
    }
    this.isMobile = /iPhone|iPad|iPod|Android|MeeGo|PlayBook/i.test(navigator.userAgent);
  }
  parent: HTMLElement;
  sections: Array<HTMLElement> = [];
  number: number = 0;
  current: number = 0;
  wait: boolean = false;
  Enable: boolean = true;
  btnGoTop;
  fbNavbar;
  navigators = [];
  scrollSeeMore;
  ngAfterViewInit(): void {
    if (!this.isMobile && this.Enable) {
      this.parent.querySelectorAll('.fullpage-section .section').forEach((section: HTMLElement, index) => {
        this.sections.push(section);
        section.classList.add('section-fullpage');
        section.style.height = window.innerHeight + 'px';
        section.style.transform = `translate(0px, ${(index - this.current) * window.innerHeight}px)`;
      });
      this.number = this.sections.length;
      if (this.number > 0) {
        this.fbNavbar = document.querySelector('.fullpage-navbar') as HTMLElement;
        var navigators = document.querySelector('.fullpage-navbar .fullpage-navigators') as HTMLElement;
        this.fbNavbar.style.display = 'block';
        for (let i = 0; i < this.number; i++) {
          var navigator = document.createElement('DIV');
          navigator.className = (i == this.current) ? 'fullpage-navigator active' : 'fullpage-navigator';
          navigator.setAttribute('data-index', i.toString());
          navigator.innerHTML = '<div class="circle"></div><div class="text">' + this.sections[i].getAttribute('data-title') + '</div>';
          navigators.appendChild(navigator);
          this.navigators.push(navigator);
        }
        this.changeNavigator();
        this.sections[0].classList.add('active');
        this.initScrollSeeMore()
        this.sections[this.sections.length - 1].appendChild(this.scrollSeeMore);
      }
    }
    this.btnGoTop = this.parent.querySelector('.btn-top-fixed');
  }
  initScrollSeeMore() {
    this.scrollSeeMore = document.createElement('DIV');
    this.scrollSeeMore.className = 'fullpage-seemore';
    this.scrollSeeMore.innerHTML = '<span>Scroll To See More</span><span><i class="fa fa-angle-double-down"></i></span>';
  }
  @HostListener('wheel', ['$event'])
  onwheel(e) {
    if (!this.isMobile) {
      if (e.deltaY > 0) {
        if (this.current < this.number - 1) {
          e.preventDefault();
          if (!this.wait) {
            this.wait = true;
            this.current++;
            this.changeSlide();
            this.changeNavigator();
            setTimeout(() => { this.wait = false; }, 1000);
          }
        }
        if (this.current >= this.number - 1 && this.wait) {
          e.preventDefault();
        }
        if (this.current >= this.number - 1 && window.scrollY > 0) {
          this.scrollSeeMore.classList.add('hide');
          this.fbNavbar.classList.add('hide');
        }
      }
      if (e.deltaY < 0) {
        if (this.current > 0 && window.scrollY == 0) {
          e.preventDefault();
          window.scrollTo(0, 0);
          this.fbNavbar.classList.remove('hide');
          if (!this.wait) {
            this.wait = true;
            this.current--;
            this.changeSlide();
            this.changeNavigator();
            setTimeout(() => { this.wait = false; }, 1000);
          }
        }
        if (window.scrollY <= 100) {
          this.fbNavbar.classList.remove('hide');
        }
      }
    }
  }
  changeNavigator() {
    this.navigators.forEach(nav => {
      nav.classList.remove('active');
    });
    this.navigators[this.current].classList.add('active');
  }
  changeSlide() {
    this.sections.forEach((section, index) => {
      section.classList.remove('active');
      section.style.transform = `translate(0px, ${-(this.current) * window.innerHeight}px)`;
    });
    this.sections[this.current].classList.add('active');
  }
  @HostListener('window:click', ['$event'])
  onNavigate(e) {
    if (e.target.closest('.btn-top-fixed')) {
      this.current = 0;
      this.changeNavigator();
      this.changeSlide();
      this.fbNavbar.classList.remove('hide');
    }
    if (e.target.closest('.fullpage-navbar .fullpage-navigator')) {
      if (!e.target.classList.contains('active')) {
        this.current = parseInt(e.target.getAttribute('data-index'));
        this.changeSlide();
        this.changeNavigator();
      }
    }
  }
  @HostListener('window:keydown', ['$event'])
  onkeydown(e) {
    if (!this.isMobile && this.Enable) {
      var code = e.keyCode;
      /** Key Down */
      if (code == 34) {
        if (this.current < this.number - 1) {
          e.preventDefault();
          this.current++;
          this.changeSlide();
          this.changeNavigator();
        } else {
          this.fbNavbar.classList.add('hide');
        }
      }
      /** Key up */
      if (code == 33) {
        if (window.scrollY - window.innerHeight <= 50 ){
          this.fbNavbar.classList.remove('hide');
        }
        if (this.current > 0 && window.scrollY == 0) {
          e.preventDefault();
          this.current--;
          this.changeSlide();
          this.changeNavigator();
        }
      }
      /** End */
      if (code == 35) {
        if (this.current < this.number - 1) {
          e.preventDefault();
          this.current = this.number - 1;
          this.changeSlide();
          this.changeNavigator();
        }
      }
      /** Home */
      if (code == 36) {
        if (this.current > 0 && window.scrollY == 0) {
          e.preventDefault();
          window.scrollTo(0, 0);
          this.current = 0;
          this.changeSlide();
          this.changeNavigator();
          this.fbNavbar.classList.remove('hide');
        }
      }
    }
  }
}
