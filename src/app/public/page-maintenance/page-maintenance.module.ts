import { PageMaintenanceComponent } from './page-maintenance.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PageMaintenanceComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageMaintenanceComponent
      }
    ])
  ]
})
export class PageMaintenanceModule { }
