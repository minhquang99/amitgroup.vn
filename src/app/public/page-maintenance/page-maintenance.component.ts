import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
@Component({
	selector: 'app-page-maintenance',
	templateUrl: './page-maintenance.component.html',
	styleUrls: ['./page-maintenance.component.scss']
})
export class PageMaintenanceComponent implements OnInit {

	constructor(private location: Location, private titleService: Title) {
		this.titleService.setTitle('Maintenance - Amitgroup - Transform Digi Together');
	}

	ngOnInit() {
	}
	backPrevious() {
		this.location.back();
	}
}
