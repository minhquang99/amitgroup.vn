import { Component, OnInit } from '@angular/core';

import { MatDialog, MatDialogRef } from '@angular/material';
@Component({
	selector: 'app-message',
	templateUrl: './message.component.html',
	styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
	public title: string;
	public message: string;
	constructor(private dialogRef: MatDialogRef<MessageComponent>) { }

	ngOnInit() {
	}
}
