import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { DemoService } from '../../services/demo.service';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'public-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('menu', [
      state('desktop', style({
        height: 'auto',
        opacity: 1
      })),
      state('show', style({
        height: '360px',
        opacity: '1',
      })),
      state('hide', style({
        height: '0px',
        opacity: '0'
      })),
      transition('hide => show', [
        animate('300ms ease-out')
      ]),
      transition('show => hide', [
        animate('300ms ease-out')
      ])
    ])
  ]
})
export class HeaderComponent implements OnInit, AfterViewInit {
  @ViewChild('main_menu', { read: ElementRef, static: false }) mainMenu: ElementRef;
  @ViewChild('navbar', { read: ElementRef, static: false }) navbar: ElementRef;
  @ViewChild('breadcrumb_bar', { read: ElementRef, static: false }) breadcrumb_bar: ElementRef;
  menu;
  logo;
  isHome = true;
  breadcrumbs;
  title;
  navbarHide = false;
  menuState = (window.innerWidth > 1024) ? 'desktop' : 'hide';
  constructor(private demo: DemoService, private BcService: BreadcrumbService, private titleService: Title) {
    this.BcService.navigationEnd$.subscribe(breadscrumbs => {
      this.breadcrumbs = breadscrumbs;
      this.title = (this.breadcrumbs[this.breadcrumbs.length - 1].label == 'About Us') ? 'Our Leadership' : this.breadcrumbs[this.breadcrumbs.length - 1].label;
      this.titleService.setTitle(this.title + ' - Amitgroup - Transform Digi Together');
      if (this.breadcrumbs[this.breadcrumbs.length - 1].label != 'Home') {
        this.isHome = true;
      } else {
        this.isHome = false;
      }
      if (this.menuState == 'show') {
        this.menuState = 'hide';
      }
    });
  }
  ngOnInit() {
    this.demo.getMenu().subscribe(menu => {
      this.menu = menu;
    });
    this.demo.getLogo().subscribe(logo => this.logo = logo);
  }
  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    if (window.innerWidth > 1024 && this.breadcrumb_bar && window.document.scrollingElement.scrollTop > 30) {
      this.breadcrumb_bar.nativeElement.style.marginTop = this.navbar.nativeElement.offsetHeight + 'px';
    }
  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    if (window.innerWidth > 1024) {
      if (!document.querySelector('.fullpage-section')) {
        if (window.scrollY > 30) {
          this.navbar.nativeElement.classList.add('fixed-top');
          if (this.breadcrumb_bar) {
            // this.breadcrumb_bar.nativeElement.style.marginTop = this.navbar.nativeElement.offsetHeight + 'px';
          }
        } else {
          this.navbar.nativeElement.classList.remove('fixed-top');
          if (this.breadcrumb_bar) {
            this.breadcrumb_bar.nativeElement.style.marginTop = '94px';
          }
        }
      }
    }
  }
  @HostListener('window:wheel', ['$event'])
  onWheel(event) {
    if (window.innerWidth > 1024) {
      if (document.querySelector('.fullpage-section')) {
        if (event.deltaY > 0 && !this.navbar.nativeElement.classList.contains('fixed-top')) {
          this.navbar.nativeElement.classList.add('fixed-top');
        }
        if (event.deltaY < 0 && this.navbar.nativeElement.classList.contains('fixed-top') && document.querySelectorAll('.fullpage-section .section')[0].classList.contains('active')) {
          this.navbar.nativeElement.classList.remove('fixed-top');
        }
      }
    }
  }
  @HostListener('window:keydown', ['$event'])
  onkeydown(e) {
    if (window.innerWidth > 1024 && document.querySelector('.fullpage-section')) {
      var code = e.keyCode;
      /** Key Down */
      if (code == 34) {
        this.navbar.nativeElement.classList.add('fixed-top');
      }
      /** Key up */
      if (code == 33) {
        if (window.scrollY == 0 && this.navbar.nativeElement.classList.contains('fixed-top') && document.querySelector('.fullpage-section .section-our-services.current')) {
          this.navbar.nativeElement.classList.remove('fixed-top');
        }
      }
      /** End */
      if (code == 35) {
        this.navbar.nativeElement.classList.add('fixed-top');
      }
      /** Home */
      if (code == 36) {
        if (window.scrollY == 0 && this.navbar.nativeElement.classList.contains('fixed-top')) {
          this.navbar.nativeElement.classList.remove('fixed-top');
        }
      }
    }
  }
  @HostListener('window:resize', ['$event'])
  onWindowResize(event) {
    if (this.menuState != 'desktop' && window.innerWidth > 1024) {
      this.menuState = 'desktop';
    }
    if (this.menuState == 'desktop' && window.innerWidth < 1024) {
      this.menuState = 'hide';
    }
  }
  @HostListener('window:click', ['$event'])
  onWindowClick(event) {
    if (window.innerWidth <= 1024) {
      if (!event.target.closest('.nav.navbar')) {
        if (this.menuState == 'show') {
          this.menuState = 'hide';
        }
      }
    }
  }
  toggleMenu(event) {
    if (window.innerWidth <= 1024) {
      this.menuState = (this.menuState == 'hide') ? 'show' : 'hide';
    } else {
      this.menuState = 'show';
    }
  }
}
