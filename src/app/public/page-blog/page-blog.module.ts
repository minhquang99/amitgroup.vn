import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageBlogComponent } from './page-blog.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PageBlogComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageBlogComponent
      }
    ])
  ]
})
export class PageBlogModule { }
