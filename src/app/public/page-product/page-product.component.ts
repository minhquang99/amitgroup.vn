import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SharedService } from '../shared.service';
import { ProductService } from '../services/product.service';

@Component({
	selector: 'app-page-product',
	templateUrl: './page-product.component.html',
	styleUrls: ['./page-product.component.scss']
})
export class PageProductComponent implements OnInit {

	products = [];
	filtedProducts = [];
	categories;
	filterCat = 0;
	imgWidth;
	constructor(private shared: SharedService, private productService: ProductService, private cdRef: ChangeDetectorRef) { }

	ngOnInit() {
		this.productService.createMockProducts().subscribe( result => { this.products = result; });
		this.categories = this.productService.mockCategory;
	}
	filterCategory(event, id) {
		this.filtedProducts = [];
		this.filterCat = id;
		this.products.forEach( (product) => {
			if(product.category == this.filterCat) {
				this.filtedProducts.push(product);
			}
		});
	}
	productImageLoaded(event) {
		let img: HTMLElement = event.target;
		let inner: HTMLElement = event.target.parentNode.querySelector('.product-inner');
		inner.style.height = img.offsetHeight + 'px';
		inner.style.width = img.offsetWidth + 'px';
	}
}