import { PageContactComponent } from './page-contact.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    PageContactComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageContactComponent
      }
    ]),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PageContactModule { }
