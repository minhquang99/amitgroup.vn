import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DemoService } from '../services/demo.service';
import { MessageService } from '../services/message.service';
import { ApiService } from '../services/api.service';
@Component({
	selector: 'app-page-contact',
	templateUrl: './page-contact.component.html',
	styleUrls: ['./page-contact.component.scss']
})
export class PageContactComponent implements OnInit {
	formdata;
	name;
	phone;
	company;
	email;
	message;
	isSending = false;
	constructor(public demo: DemoService, private messageService: MessageService, private api: ApiService) {
	}

	ngOnInit() {
		this.formdata = new FormGroup({
			name: new FormControl('', Validators.compose([
				Validators.required,
			])),
			phone: new FormControl('', Validators.compose([
				Validators.required,
				this.formatPhone
			])),
			company: new FormControl('', Validators.compose([
				Validators.required,
			])),
			email: new FormControl('', Validators.compose([
				Validators.required,
				this.formatEmail
			])),
			message: new FormControl('', Validators.compose([
			])),
		});
	}
	formatEmail(control) {
		var email = control.value.trim();
		if (email != '') {
			if (!email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)) {
				return {
					formatEmailInvalid: true
				};
			}
		}
	}
	formatPhone(control) {
		var phone = control.value.trim();
		var regex = /^\+[1-9]{1}[0-9]{3,14}$/;
		if (phone != '') {
			if (!phone.match(regex)) {
				return {
					formatPhoneInvalid: true
				};
			}
		}
	}
	onSubmit(data) {
		if (!this.isSending) {
			if (this.formdata.invalid) {
				var message = '';
				for (const name in this.formdata.controls) {
					var control = this.formdata.controls[name];
					if (control.invalid) {
						control.touched = true;
						message += `${name} is invalid.\n`;
					}
				}
				this.messageService.openDialog('Error', message).subscribe();
				return false;
			}
			this.isSending = true;
			this.api.sendMail('company-contact', data).subscribe(result => {
				if (result) {
					this.messageService.openDialog('Success', result.message);
					this.resetFormData();
				}
				this.isSending = false;
			});
		}
	}
	resetFormData(): void {
		this.formdata.get('name').touched = false;
		this.formdata.get('email').touched = false;
		this.formdata.get('phone').touched = false;
		this.formdata.get('message').touched = false;
		this.formdata.get('company').touched = false;
		this.formdata.get('name').setValue('');
		this.formdata.get('email').setValue('');
		this.formdata.get('phone').setValue('');
		this.formdata.get('message').setValue('');
		this.formdata.get('company').setValue('');
	}
}
