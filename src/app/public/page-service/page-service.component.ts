import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../services/service.service';

@Component({
	selector: 'app-page-service',
	templateUrl: './page-service.component.html',
	styleUrls: ['./page-service.component.scss']
})
export class PageServiceComponent implements OnInit {
	services = [];
	constructor(private serviceService: ServiceService) {
	}

	ngOnInit() {
		if(this.services.length <= 0) {
			this.serviceService.createDemoServices().subscribe( services => {
				this.services = services;
			});
		}
	}

}
