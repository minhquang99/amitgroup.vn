import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PageServiceComponent } from './page-service.component';

@NgModule({
  declarations: [
    PageServiceComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageServiceComponent
      }
    ])
  ]
})
export class PageServiceModule { }
